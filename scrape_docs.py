#!/usr/bin/env python3

import argparse
import sys


def read_input_lines(fnames):
    lines = []
    for fname in fnames:
        if fname == "-":
            lines.extend(sys.stdin.readlines())
        with open(fname, 'rt') as f:
            lines.extend(f.readlines())
    return lines


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="input header file '-' for stdin", default="-", nargs="+")
    parser.add_argument("-o", "--output", help="output file '-' for stdout", default="-")
    parser.add_argument("-g", "--guard", help="preprocessor guard phrase", default="AUTO_DOCS")
    parser.add_argument("-n", "--namespace", help="namespace to put docs in", default="docs")
    return parser.parse_args()


def clean_tag_name(tag):
    return tag.replace(".", "_").replace("::", "__")


def process_input(input_lines, output_file, namespace="", guard="AUTO_DOCS"):
    in_docs = False
    block_assignment = None
    block_lines = []

    substitutions = {
        '@brief ': '',
        '@details': '\n',
        '@detail': '\n',
        '@note': '\nNote:',
        '@return': 'returns',
        '@param': 'parameter',
    }

    tag_str = '@noop tag '

    output_file.write("""
#ifndef {0}
#define {0}

""".format(guard))
    if namespace != "":
        output_file.write("namespace {0}\n{1}\n".format(namespace, "{"))

    output_file.write("""
// clang-format off
// clang-format does not handle C++ raw strings well, so don't format them.
""")

    for line in input_lines:
        line = line.strip()
        if in_docs and line.startswith('*/'):
            in_docs = False
            if block_assignment is not None:
                output_file.write("\n\tconst char* doc_string_{0} = R\"docstring(\n".format(clean_tag_name(block_assignment)))
                for cur_line in block_lines:
                    output_file.write(cur_line + '\n')
                output_file.write(")docstring\";\n")
            block_assignment = None
            block_lines = []

        if in_docs:
            if line.startswith('* '):
                line = line[2:]
            for sub in substitutions:
                line = line.replace(sub, substitutions[sub])

            if line.startswith(tag_str):
                block_assignment = line[len(tag_str):]
            else:
                block_lines.append(line)
        if line.startswith('/**') or line.startswith('/*!'):
            in_docs = True

    output_file.write("""
// clang-format on
""")
    if namespace != "":
        output_file.write("{0}\n".format("}"))
    output_file.write("\n#endif /* {0} */\n".format(guard))


opts = parse_args()


def process(out):
    global opts
    process_input(read_input_lines(opts.input), out, namespace=opts.namespace, guard=opts.guard)


if opts.output == '-':
    process(sys.stdout)
else:
    with open(opts.output, 'wt') as output:
        process(output)

#!/usr/bin/envy python3

import os
import struct
import numpy as np

import daqd_stream

print(dir(daqd_stream))

chan = daqd_stream.online_channel()
chan.name = "H1:PSL-AUX-1"
print("{0}".format(chan))

print(dir(chan))

client = daqd_stream.client("daqd_stream")
print(dir(client))
chans = []
i = 0
for chan in client.channels():
    i = i + 1
    # print(chan)
    if len(chans) < 5 and i % 23 == 0:
        print(chan.name)
        chans.append(chan.name)

print(chans)

plan = client.plan_request(daqd_stream.PLAN_TYPE.PLAN_16TH, chans)
count = plan.required_size_16th()
buf = np.zeros(count, np.int8)

client.get_16th_data(plan, buf)
print(buf)
print(len(buf))
print(struct.unpack("5i", buf))



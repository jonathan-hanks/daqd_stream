import contextlib
import integration
import os.path
import time

import requests

import daqd_stream

integration.Executable(name="daqd_stream_srv", hints=['../../src/apps/daqd_stream_server'], description="daqd stream server")
integration.Executable(name="fe_simulated_streams",
                       hints=["/usr/bin", ],
                       description="data server")
integration.Executable(name="cps_xmit",
                       hints=["/usr/bin", ],
                       description="data xmit")
integration.Executable(name="cds_metadata_server", hints=["/usr/bin"], description="channel metadata server")

ini_dir = integration.state.temp_dir('inis')
master_file = os.path.join(ini_dir, 'master')

generator = integration.Process("fe_simulated_streams",
                                ["-b", "local_dc",
                                 "-m", "100",
                                 "-k", "100",
                                 "-R", "5",
                                 "-i", ini_dir,
                                 "-M", master_file,
                                 "-admin", "127.0.0.1:10000",
                                 "-s", "shm://",
                                 ])
xmit = integration.Process("cps_xmit",
                           ["-b", "shm://local_dc",
                            "-m", "100",
                            "-p", "tcp://127.0.0.1:9000"],)
metadata = integration.Process("cds_metadata_server",
                               ["-listen", "127.0.0.1:9100",
                                "-master", master_file, ])
server = integration.Process("daqd_stream_srv", [
    "-d", "tcp://127.0.0.1:9000|meta://ini://" + master_file
])


def fail():
    raise RuntimeError("Fail here")


@contextlib.contextmanager
def cleanup_shmem(segments=('local_dc', 'daqd_stream',)):
    yield
    for entry in segments:
        path = os.path.join('/dev/shm', entry)
        try:
            os.remove(path)
        except:
            pass


channel_lists = {}
config_checksums = {}


def wait_for_channels():
    def wrapper():
        client = daqd_stream.client("daqd_stream")
        tries = 0
        while tries < 20:
            tries += 1
            channels = client.channels()
            if len(channels) > 0:
                return
            time.sleep(.5)
        raise RuntimeError("timeout waiting for channels")

    return wrapper


def get_channels(dest):
    def wrapper():
        global channel_lists
        global config_checksums

        client = daqd_stream.client("daqd_stream")
        channel_lists[dest] = client.channels()
        config_checksums[dest] = client.config_checksum()
        del client

    return wrapper


def mutate_dcuid(dcuid):
    def do_mutate():
        resp = requests.post("http://localhost:10000/api/v1/dcu/{0}/mutate/".format(dcuid),
                             data={'DcuId': dcuid, 'RemoveCount': 100, 'AddCount': 150,
                                   'RemoveChannels': '', 'AddChannels': '', 'ProtectedChannels': ''})
        if resp.status_code != requests.codes.ok:
            raise RuntimeError("Unexpected return from the mutate request {0}: {1}".format(resp.status_code, resp.text))
        time.sleep(1.0)
        resp = requests.post("http://localhost:10000/api/v1/dcu/{0}/start/".format(dcuid))
        if resp.status_code != requests.codes.ok:
            raise RuntimeError("Unexpected return from the start request {0}: {1}".format(resp.status_code, resp.text))
    return do_mutate


def require_differing_channel_lists():
    global channel_lists
    global config_checksums

    if len(channel_lists[1]) == len(channel_lists[2]):
        difference = False
        for i in range(len(channel_lists[1])):
            if str(channel_lists[1][i]) != str(channel_lists[2][i]):
                difference = True
                break
        if not difference:
            raise RuntimeError("Identical channel lists")
    if config_checksums[1] == config_checksums[2]:
        raise RuntimeError("Identical config checksums")


wait = integration.callable(time.sleep, 3.0)
with cleanup_shmem():
    integration.Sequence(
        [
            # integration.state.preserve_files,
            generator.run,
            integration.wait_tcp_server(port=10000, timeout=5),
            xmit.run,
            integration.wait_tcp_server(port=9000, timeout=5),
            metadata.run,
            integration.wait_tcp_server(port=9100, timeout=5),
            server.run,
            wait,
            wait_for_channels(),
            get_channels(1),
            mutate_dcuid(5),
            wait,
            get_channels(2),
            server.ignore,
            metadata.ignore,
            xmit.ignore,
            generator.ignore,
            require_differing_channel_lists,
         ]
    )
print("done!")

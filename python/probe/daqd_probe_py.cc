//
// Created by jonathan.hanks on 9/27/22.
//

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

#include <iterator>
#include <memory_buffer.hh>
#include "internal.hh"
#include "multi_source_info_block.hh"

#include <memory>

namespace
{
    class Buffer;

    using parent_t = std::shared_ptr<Memory_buffer>;

    static const char*
    dtype_to_string( daqd_stream::DATA_TYPE data_type )
    {
        using daqd_stream::DATA_TYPE;

        switch ( data_type )
        {
        case DATA_TYPE::INT16:
            return "int_2";
        case DATA_TYPE::INT32:
            return "int_4";
        case DATA_TYPE::INT64:
            return "int_8";
        case DATA_TYPE::FLOAT32:
            return "real_4";
        case DATA_TYPE::FLOAT64:
            return "real_8";
        case DATA_TYPE::UINT32:
            return "uint_4";
        default:
            return "unknown";
        }
    }

    struct signal_units
    {
        signal_units( double      signal_offset,
                      double      signal_slope,
                      std::string signal_units )
            : offset( signal_offset ), slope( signal_slope ),
              units( std::move( signal_units ) )
        {
        }

        double      offset;
        double      slope;
        std::string units;
    };

    std::string
    read_name( const daqd_stream::online_channel& chan )
    {
        return std::string( chan.name.data( ) );
    }

    std::string
    repr( const daqd_stream::online_channel& chan )
    {
        return "<" + read_name( chan ) + ">";
    }

    std::string
    read_units( const daqd_stream::online_channel& chan )
    {
        return std::string( chan.units.data( ) );
    }

    struct shared_mem_header {
        parent_t parent_;
        daqd_stream::detail::shared_mem_header* header;
    };

    struct multi_source_info_block {
        parent_t parent_;
        daqd_stream::detail::multi_source_info_block* block;
    };

    struct data_block {
        parent_t parent_;
        daqd_stream::detail::data_block* block;
    };

    struct ifo_config
    {
        daqd_stream::detail::ifo_checksums checksums{};
        std::vector<daqd_stream::online_channel> channels{};
    };

    ifo_config
    get_config(shared_mem_header& m)
    {
        ifo_config cfg;
        auto shared_cfg = m.header->get_config();
        cfg.checksums = shared_cfg->checksums;
        cfg.channels.reserve(shared_cfg->channels.size());
        std::copy(shared_cfg->channels.begin(), shared_cfg->channels.end(),
                   std::back_inserter(cfg.channels));
        return cfg;
    }

    class Buffer {
    public:
        explicit Buffer(const std::string& path): buf_{std::make_shared<Memory_buffer>(path)} {}

        shared_mem_header
            as_shared_mem_header()
        {
            return shared_mem_header{buf_, buf_->get_as<daqd_stream::detail::shared_mem_header>()};
        }

        multi_source_info_block
            as_multi_source_info_block()
        {
            return multi_source_info_block{buf_, buf_->get_as<daqd_stream::detail::multi_source_info_block>()};
        }
    private:
        std::shared_ptr<Memory_buffer> buf_{};
    };
}

namespace py = pybind11;

PYBIND11_MODULE( daqd_probe, m )
{
    m.doc( ) = "tools to help probe a daqd_stream memory buffer";

    py::class_< daqd_stream::online_channel >(
        m, "online_channel" )
        .def_property_readonly( "name", read_name )
        .def_readwrite( "dcuid", &daqd_stream::online_channel::dcuid )
        .def_readwrite( "datatype", &daqd_stream::online_channel::datatype )
        .def_readwrite( "datarate", &daqd_stream::online_channel::datarate )
        .def_readwrite( "bytes_per_16th",
                        &daqd_stream::online_channel::bytes_per_16th )
        .def_readwrite( "data_offset",
                        &daqd_stream::online_channel::data_offset )
        .def_readwrite( "signal_gain",
                        &daqd_stream::online_channel::signal_gain )
        .def_readwrite( "signal_slope",
                        &daqd_stream::online_channel::signal_slope )
        .def_readwrite( "signal_offset",
                        &daqd_stream::online_channel::signal_offset )
        .def_property_readonly( "units", read_units )
        .def( "__repr__", repr )
        .def( "__getitem__",
              []( const daqd_stream::online_channel& channel,
                  const std::string                  index ) -> py::object {
                  if ( index == "channel_type" )
                  {
                      return py::cast( "online" );
                  }
                  else if ( index == "data_type" )
                  {
                      return py::cast( dtype_to_string( channel.datatype ) );
                  }
                  else if ( index == "rate" )
                  {
                      return py::cast( channel.datarate );
                  }
                  else if ( index == "units" )
                  {
                      signal_units units( channel.signal_offset,
                                          channel.signal_slope,
                                          channel.units.data( ) );
                      return py::cast( units );
                  }
                  throw std::out_of_range( index );
              } );

    py::class_<multi_source_info_block>(m, "multi_source_info_block")
        .def_property_readonly("magic", [](const multi_source_info_block& m) -> std::int32_t {
            return m.block->magic;
        })
        .def_property_readonly("version", [](const multi_source_info_block& m) {
            return m.block->version;
        })
        .def_property_readonly("block_range", [](const multi_source_info_block& m) {
            return m.block->block_range;
        })
        .def("sources", [](const multi_source_info_block& m, int i) -> std::string {
            return std::string(m.block->sources[i].data());
        });

    py::class_<data_block>(m, "data_block")
        .def_property_readonly("gps_second", [](const data_block& db) -> std::uint64_t {
            return db.block->gps_second;
        })
        .def_property_readonly("cycle", [](const data_block& db) -> std::uint64_t {
            return db.block->cycle;
        })
        .def_property_readonly("dcu_offsets", [](const data_block& db) {
            return db.block->dcu_offsets;
        })
        .def_property_readonly("dcu_status", [](const data_block& db) {
            return db.block->dcu_status;
        })
        .def_property_readonly("dcu_config", [](const data_block& db) {
            return db.block->dcu_config;
        })
        .def("data", [](const data_block& db) {
            return py::array_t<char>(sizeof(db.block->data, db.block->data));
        });

    py::class_<daqd_stream::detail::ifo_checksums>(m, "ifo_checksums")
        .def_readonly("master_config", &daqd_stream::detail::ifo_checksums::master_config)
        .def_readonly("dcu_config", &daqd_stream::detail::ifo_checksums::dcu_config);

    py::class_<ifo_config>(m, "ifo_config")
        .def_readonly("checksums", &ifo_config::checksums)
        .def_readonly("channels", &ifo_config::channels);

    py::class_<shared_mem_header>(m, "shared_mem_header")
        .def_property_readonly("magic", [](const shared_mem_header& m) -> std::uint32_t {
            return m.header->magic_bytes;
        })
        .def_property_readonly("struct_id", [](const shared_mem_header& m) -> std::uint32_t {
            return m.header->struct_id;
        })
        .def_property_readonly("version", [](const shared_mem_header& m) -> std::uint32_t {
            return m.header->version;
        })
        .def_property_readonly("cur_block", [](const shared_mem_header& m) -> std::int32_t {
            return m.header->cur_block;
        })
        .def_property_readonly("num_blocks", [](const shared_mem_header& m) -> std::uint32_t {
            return m.header->num_blocks;
        })
        .def_property_readonly("segment_size", [](const shared_mem_header& m) -> std::uint64_t {
            return m.header->segment_size;
        })
        .def("blocks", [](const shared_mem_header& m, int index) -> data_block  {
            return data_block{m.parent_, &m.header->blocks(index)};
        })
        .def( "get_config", get_config);

    py::class_<Buffer>(m, "buffer")
        .def( py::init< std::string> ())
        .def( "shared_mem_header", &Buffer::as_shared_mem_header)
        .def( "multi_source_info_block", &Buffer::as_multi_source_info_block);
}


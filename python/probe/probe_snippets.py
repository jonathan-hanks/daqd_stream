import typing
import numpy as np
import daqd_probe as dp


def open(name:str) -> dp.buffer:
    return dp.buffer(name)


def get_channel(b:dp.buffer, name:str) -> dp.online_channel:
    for chan in get_channels(b):
        if chan.name == name:
            return chan
    raise KeyError("Unable to find {0}".format(name))


def get_channels(b:dp.buffer) -> typing.List:
    header = b.shared_mem_header()
    cfg = header.get_config()
    return cfg.channels


def find_block_index(header: dp.shared_mem_header, gps: int, cycle: int) -> int:
    for i in range(header.num_blocks):
        db = header.blocks(i)
        if db.gps_second == gps and db.cycle == cycle:
            return i
    raise KeyError("Timespan not found {0}:{1}".format(gps, cycle))


def get_data(b:dp.buffer, chan:dp.online_channel, gps:int):
    header = b.shared_mem_header()
    start = find_block_index(header, gps, 0)
    end = find_block_index(header, gps, 15)
    max = header.num_blocks
    cur = start

    data = b''
    while cur != end:
        block = header.blocks(cur)

        block_config = block.dcu_config[chan.dcuid]
        block_status = block.dcu_status[chan.dcuid]
        block_offset = block.dcu_offsets[chan.dcuid]

        if block_status == 2:
            data = data + block.data()[ block_offset + chan.data_offset : block_offset + chan.data_offset + chan.bytes_per_16th].tobytes()
        else:
            print("skipping block {0} due to bad status".format(cur))
            data = data + b'0'*chan.bytes_per_16th

        cur += 1
        if cur >= max:
            cur = 0
    return data

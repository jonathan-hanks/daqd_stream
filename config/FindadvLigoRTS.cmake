find_path(advLigoRTS_INCLUDE_DIRECTORIES advligorts/daq_core.h
        HINTS /usr/include /usr/local/include)

find_file(advLigoRTS_HAS_METADATA daq_metadata.h
        PATHS ${advLigoRTS_INCLUDE_DIRECTORIES})
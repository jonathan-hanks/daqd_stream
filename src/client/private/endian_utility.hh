//
// Created by jonathan.hanks on 2/10/21.
//

#ifndef DAQD_STREAM_UTILITY_HH
#define DAQD_STREAM_UTILITY_HH

#include <daqd_stream/daqd_stream.hh>

#include "client_internal.hh"

namespace daqd_stream
{
    namespace detail
    {
        void
        endian_convert_in_place( void*       buffer,
                                 std::size_t buffer_size,
                                 const std::vector< output_location >& metadata,
                                 std::size_t                           cycles,
                                 byte_order                            target );

        inline void
        endian_convert_in_place( void*            buffer,
                                 const data_plan& plan,
                                 std::size_t      cycles,
                                 byte_order       target )
        {
            const_plan_internals intl{ plan };
            endian_convert_in_place( buffer,
                                     plan.required_size( ),
                                     intl.output_metadata( ),
                                     cycles,
                                     target );
        }
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_UTILITY_HH

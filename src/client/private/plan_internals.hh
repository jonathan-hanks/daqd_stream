//
// Created by jonathan.hanks on 2/24/21.
//

#ifndef DAQD_STREAM_PLAN_INTERNALS_HH
#define DAQD_STREAM_PLAN_INTERNALS_HH

#include <daqd_stream/daqd_stream.hh>

namespace daqd_stream
{
    namespace detail
    {
        struct plan_internals
        {
            static const std::vector< output_location >&
            get_locations( const data_plan& plan )
            {
                return plan.output_metadata_;
            }

            explicit plan_internals( data_plan& plan ) : plan_{ plan }
            {
            }

            checksum_t&
            global_config( ) noexcept
            {
                return plan_.global_config_checksum_;
            }
            channel_name_list&
            channels( ) noexcept
            {
                return plan_.channels_;
            }
            std::vector< dcu_span >&
            segments( ) noexcept
            {
                return plan_.segments_;
            }
            std::vector< output_location >&
            output_metadata( ) noexcept
            {
                return plan_.output_metadata_;
            }
            std::vector< data_plan::dcu_checksum >&
            dcu_configs( ) noexcept
            {
                return plan_.dcu_configs_;
            }
            bool
            multi_source( ) const noexcept
            {
                return plan_.multi_source_;
            }

            data_plan& plan_;
        };

        struct const_plan_internals
        {
            static const std::vector< output_location >&
            get_locations( const data_plan& plan )
            {
                return plan.output_metadata_;
            }

            explicit const_plan_internals( const data_plan& plan )
                : plan_{ plan }
            {
            }

            checksum_t
            global_config( ) const noexcept
            {
                return plan_.global_config_checksum_;
            }
            const channel_name_list&
            channels( ) const noexcept
            {
                return plan_.channels_;
            }
            const std::vector< dcu_span >&
            segments( ) const noexcept
            {
                return plan_.segments_;
            }
            const std::vector< output_location >&
            output_metadata( ) const noexcept
            {
                return plan_.output_metadata_;
            }
            const std::vector< data_plan::dcu_checksum >&
            dcu_configs( ) const noexcept
            {
                return plan_.dcu_configs_;
            }
            bool
            multi_source( ) const noexcept
            {
                return plan_.multi_source_;
            }

            const data_plan& plan_;
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_PLAN_INTERNALS_HH

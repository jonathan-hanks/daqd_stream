/*
Copyright 2022 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_CLIENT_INTERNAL_HH
#define DAQD_STREAM_CLIENT_INTERNAL_HH

#include <array>
#include <vector>

#include "internal.hh"
#include "plan_internals.hh"

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief a helper class that provides data_plans with access to the low
         * level buffer data.
         * @note This is used to help keep the internal structures and
         * interfaces isolated from the public interface.
         */
        struct buffer_offset_helper
        {
            explicit buffer_offset_helper(
                const data_block&                     db,
                const std::vector< output_location >& dest_info,
                checksum_t                            global_config_checksum )
                : dcu_offsets{ &db.dcu_offsets }, dcu_status{ &db.dcu_status },
                  dcu_configs{ &db.dcu_config }, dest_information{ &dest_info },
                  data{ db.data }, global_config{ global_config_checksum }
            {
            }

            const volatile char*
            buffer( std::size_t index ) const
            {
                return data + ( *dcu_offsets )[ index ];
            }

            std::uint32_t
            status( std::size_t index ) const
            {
                return ( *dcu_status )[ index ];
            }

            checksum_t
            config( std::size_t index ) const
            {
                return ( *dcu_configs )[ index ];
            }

            const std::array< std::size_t, max_dcu( ) >*   dcu_offsets;
            const std::array< std::uint32_t, max_dcu( ) >* dcu_status;
            const std::array< checksum_t, max_dcu( ) >*    dcu_configs;
            const std::vector< output_location >*          dest_information;
            const volatile char*                           data;
            const checksum_t                               global_config;
        };

        inline std::function< bool( void ) >
        pred_16th( shared_mem_header* header, const time_point& last_time_seen )
        {
            return [ header, last_time_seen ]( ) -> bool {
                auto        index = header->get_cur_block( );
                data_block& cur_block = header->blocks( index );
                auto        cur_time_point = time_point_from_cycle(
                    cur_block.gps_second, cur_block.cycle );

                return cur_time_point > last_time_seen;
            };
        }

        inline std::function< bool( void ) >
        pred_sec( shared_mem_header* header, std::int64_t start_sec )
        {
            auto cur_sec =
                header->blocks( header->get_cur_block( ) ).gps_second;
            //            std::cerr << "Creating second predicate with start_sec
            //            = " << start_sec << ", now at cur_sec = " << cur_sec
            //            << "\n";
            return [ header, start_sec ]( ) -> bool {
                auto        index = header->get_cur_block( );
                data_block& cur_block = header->blocks( index );

                auto cur_time_point = time_point_from_cycle(
                    cur_block.gps_second, cur_block.cycle );
                auto required_time_point =
                    time_point_from_cycle( start_sec, 15 );

                //                std::cerr << "sec_pred cur_time_point = " <<
                //                cur_time_point.seconds
                //                          << " req_time_point = " <<
                //                          required_time_point.seconds << "\n";
                return cur_time_point >= required_time_point;
            };
        }

        inline std::int32_t
        get_start_block_index_sec( const shared_mem_header* header,
                                   std::int64_t start_time ) noexcept
        {
            auto index = std::int32_t( 0 );
            auto cur = header->get_cur_block( );
            auto target = make_time_point( start_time, 0 );

            if ( start_time == 0 )
            {
                target.seconds = header->blocks( cur ).gps_second;
            }

            auto tries = header->safe_data_blocks( );
            index = cur;

            while ( tries > 0 )
            {
                const data_block& cur_block = header->blocks( cur );
                auto cur_time = time_point_from_cycle( cur_block.gps_second,
                                                       cur_block.cycle );
                if ( cur_time >= target )
                {
                    index = cur;
                }
                --tries;
                cur = header->prev_block( cur );
            }
            return index;
        }

        /*!
         * @brief all the state a 'client' needs, a handle to the memory area
         */
        struct client_intl
        {
            explicit client_intl(
                std::shared_ptr< Posix_shared_memory > segment,
                signaling_data_block* multi_signal_block = nullptr )
                : shared_{ std::move( segment ) }, multi_signal_block_{
                      multi_signal_block
                  }
            {
            }
            explicit client_intl(
                const std::string&    segment_name,
                signaling_data_block* multi_signal_block = nullptr )
                : shared_(
                      std::make_shared< Posix_shared_memory >( segment_name ) ),
                  multi_signal_block_{ multi_signal_block }
            {
            }

            signaling_data_block&
            signal_block( ) const
            {
                if ( multi_signal_block_ )
                {
                    return *multi_signal_block_;
                }
                return shared_->get_as< detail::shared_mem_header >( )
                    ->signal_block;
            }
            std::shared_ptr< Posix_shared_memory > shared_;
            signaling_data_block* multi_signal_block_{ nullptr };
        };
        using shared_mem_block = client_intl;

        template < typename IfoConfigType >
        data_plan
        layout_data_plan( PLAN_TYPE                plan_type,
                          const channel_name_list& channel_names,
                          const IfoConfigType&     layout,
                          std::size_t              seconds_in_buffer )
        {
            std::vector< dcu_span >                segments{ };
            std::vector< output_location >         output_mapping{ };
            std::map< dcu_identifier, checksum_t > dcu_checksums;

            auto multiplier = 1;
            switch ( plan_type )
            {
            case PLAN_TYPE::PLAN_16TH:
                multiplier = 1;
                if ( seconds_in_buffer != 0 )
                {
                    throw std::runtime_error(
                        "seconds_in_buffer must be == 0" );
                }
                break;
            case PLAN_TYPE::PLAN_SEC:
                multiplier = detail::cycles_per_sec( ) * seconds_in_buffer;
                if ( seconds_in_buffer <= 0 )
                {
                    throw std::runtime_error( "seconds_in_buffer must be > 0" );
                }
                break;
            default:
                std::runtime_error( "Unknown plan type" );
            }

            segments.reserve( channel_names.size( ) );
            output_mapping.reserve( channel_names.size( ) );

            int output_offset = 0;

            for ( const auto& name : channel_names )
            {
                unsigned char source_number{ 0 };
                auto          it = find_by_name_with_source(
                    layout.channels, name, source_number );
                if ( it == layout.channels.cend( ) )
                {
                    throw std::runtime_error(
                        concatenate( "Channel not found, ", name ) );
                }
                auto&    base{ *it };
                dcu_span tmp( source_number,
                              base.dcuid,
                              base.data_offset,
                              base.bytes_per_16th );
                segments.emplace_back( tmp );
                output_mapping.emplace_back( base, output_offset );
                output_offset += base.bytes_per_16th * multiplier;
                auto cksum_it = dcu_checksums.find(
                    dcu_identifier( source_number, base.dcuid ) );
                if ( cksum_it == dcu_checksums.end( ) )
                {
                    auto cur_id = dcu_identifier( source_number, base.dcuid );
                    dcu_checksums.emplace( std::make_pair(
                        cur_id, layout.checksums.lookup_checksum( cur_id ) ) );
                }
            }
            return data_plan( plan_type,
                              channel_names,
                              std::move( segments ),
                              std::move( output_mapping ),
                              layout.checksums.master_config,
                              std::move( dcu_checksums ),
                              seconds_in_buffer );
        }

        inline std::vector< data_plan::dcu_checksum >::iterator
        find_in_config_list( std::vector< data_plan::dcu_checksum >& checksums,
                             dcu_identifier identifier )
        {
            auto it =
                std::lower_bound( checksums.begin( ),
                                  checksums.end( ),
                                  identifier,
                                  []( const data_plan::dcu_checksum cur,
                                      dcu_identifier identifier ) -> bool {
                                      return cur.identifier < identifier;
                                  } );
            if ( it == checksums.end( ) )
            {
                return it;
            }
            if ( it->identifier == identifier )
            {
                return it;
            }
            return checksums.end( );
        }

        inline bool
        configs_match( std::vector< data_plan::dcu_checksum >& checksums,
                       dcu_identifier                          identifier,
                       checksum_t                              checksum )
        {
            auto it = find_in_config_list( checksums, identifier );
            if ( it == checksums.end( ) )
            {
                return false;
            }
            return it->config == checksum;
        }

        inline bool
        good_channel_match( const online_channel&  chan,
                            const output_location& meta_data )
        {
            return ( meta_data.datatype == chan.datatype &&
                     meta_data.datarate == chan.datarate );
        }

        template < typename IfoConfigType >
        bool
        relayout_data_plan( data_plan& plan, const IfoConfigType& layout )
        {
            plan_internals internals{ plan };
            if ( internals.global_config( ) == layout.checksums.master_config )
            {
                return false;
            }
            if ( internals.channels( ).size( ) !=
                 internals.segments( ).size( ) )
            {
                throw std::runtime_error( "Invalid state for the data plan, "
                                          "segment count != channel size" );
            }

            internals.dcu_configs( ).reserve( max_dcu( ) );
            std::set< dcu_identifier > used_dcus;

            for ( auto i = 0; i < internals.channels( ).size( ); ++i )
            {
                auto&          cur_span = internals.segments( )[ i ];
                auto           cur_source = cur_span.source;
                auto           cur_dcu = cur_span.dcu;
                dcu_identifier cur_identifier{
                    static_cast< unsigned char >( cur_source ),
                    static_cast< unsigned char >( cur_dcu )
                };

                if ( !configs_match(
                         internals.dcu_configs( ),
                         cur_identifier,
                         layout.checksums.lookup_checksum( cur_identifier ) ) )
                {
                    const auto& cur_name = internals.channels( )[ i ];
                    const auto& cur_metadata =
                        internals.output_metadata( )[ i ];
                    // lookup the channel, need to match on the existing
                    // metadata
                    unsigned char source_number{ 0 };
                    auto          ch_it = find_by_name_with_source(
                        layout.channels, cur_name, source_number );
                    if ( ch_it == layout.channels.cend( ) ||
                         strcmp( ch_it->name.data( ), cur_name.c_str( ) ) !=
                             0 ||
                         !good_channel_match( *ch_it, cur_metadata ) )
                    {
                        // std::cout << "could not find channel\n";
                        // channel not found
                        internals.segments( )[ i ].source = 0;
                        internals.segments( )[ i ].dcu = 0;
                    }
                    else
                    {
                        // good match
                        internals.segments( )[ i ] =
                            dcu_span( source_number,
                                      ch_it->dcuid,
                                      ch_it->data_offset,
                                      ch_it->bytes_per_16th );
                    }
                }
                used_dcus.insert( internals.segments( )[ i ].identifier( ) );
            }
            internals.dcu_configs( ).clear( );

            for ( const auto& used_dcu : used_dcus )
            {
                // skip 0 as that is an invalid dcu
                if ( used_dcu.dcuid == 0 )
                {
                    continue;
                }
                internals.dcu_configs( ).emplace_back( data_plan::dcu_checksum{
                    used_dcu, layout.checksums.lookup_checksum( used_dcu ) } );
            }
            internals.global_config( ) = layout.checksums.master_config;

            return true;
        }

        template < typename ContainerType >
        std::vector< std::size_t >
        build_size_list( const channel_name_list& names,
                         const ContainerType      channels )
        {
            std::vector< std::size_t > sizes( names.size( ), 0 );
            auto                       out = sizes.begin( );
            for ( const auto& name : names )
            {
                auto it = std::lower_bound( channels.cbegin( ),
                                            channels.cend( ),
                                            name,
                                            detail::search_by_name( ) );
                if ( it == channels.cend( ) || it->name != name.c_str( ) )
                {
                    throw std::runtime_error(
                        concatenate( "Channel not found, ", name ) );
                }
                *out = it->bytes_per_16th;
                ++out;
            }
            return sizes;
        }

        template < typename ChannelList, typename StringLike >
        typename ChannelList::const_iterator
        find_by_name_( const ChannelList& channels, StringLike& name )
        {
            auto count = channels.size( );
            auto start = channels.cbegin( );
            auto end = channels.cend( );
            auto it =
                std::lower_bound( start, end, name, detail::search_by_name( ) );

            if ( it == channels.cend( ) || ( it->name != name.data( ) ) )
            {
                return channels.end( );
            }
            return it;
        }

        static inline std::vector< online_channel >::const_iterator
        find_by_name_with_source( const std::vector< online_channel >& channels,
                                  std::string                          name,
                                  unsigned char&                       src )
        {
            src = 0;
            return find_by_name_( channels, name );
        }

        static inline const online_channel*
        find_by_name_with_source( const shared_span< online_channel >& channels,
                                  std::string                          name,
                                  unsigned char&                       src )
        {
            src = 0;
            return find_by_name_( channels, name );
        }

        class multi_channel_list;
        static inline const online_channel* find_by_name_with_source(
            const multi_channel_list&, std::string, unsigned char& );

        class multi_channel_list
        {
        public:
            using value_type = online_channel;

            multi_channel_list( ) = default;
            multi_channel_list( const multi_channel_list& ) = default;
            multi_channel_list( multi_channel_list&& ) noexcept = default;
            multi_channel_list&
            operator=( const multi_channel_list& ) = default;
            multi_channel_list&
            operator=( multi_channel_list&& ) noexcept = default;

            void
            add_source( online_channel* source, int size )
            {
                sources_.emplace_back( source, size );
            }
            void
            add_source( shared_span< online_channel > source )
            {
                sources_.emplace_back( source );
            }

            template < typename StringLike >
            const online_channel*
            find_by_name( StringLike& name, unsigned char& dest_src ) const
            {
                int i = 0;
                for ( const auto& source : sources_ )
                {
                    unsigned char dummy{ 0 };
                    auto it = ::daqd_stream::detail::find_by_name_with_source(
                        source, name, dummy );
                    if ( it != source.end( ) )
                    {
                        dest_src = static_cast< unsigned char >( i );
                        return it;
                    }
                    ++i;
                }
                return end( );
            }

            const online_channel*
            end( ) const noexcept
            {
                return nullptr;
            }

            const online_channel*
            cend( ) const noexcept
            {
                return nullptr;
            }

            std::vector< shared_span< online_channel > > sources_{ };
        };

        static inline const online_channel*
        find_by_name_with_source( const multi_channel_list& channels,
                                  std::string               name,
                                  unsigned char&            src )
        {
            return channels.find_by_name( name, src );
        }
    } // namespace detail
} // namespace daqd_stream
#endif
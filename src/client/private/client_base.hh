#ifndef DADQD_STREAM_PRIVATE_CLIENT_BASE_HH
#define DADQD_STREAM_PRIVATE_CLIENT_BASE_HH

#include <daqd_stream/daqd_stream.hh>
#include "internal.hh"

namespace daqd_stream
{
    namespace detail
    {
        /**
         * @brief client_base sits between the public definition of a client
         * and the actual implementations to give us some place to put
         * more private implementation details.
         */
        class client_base : public client
        {
        public:
            client_base( ) : client( )
            {
            }
            explicit client_base( int assigned_source_number )
                : client( ), source_number_{ assigned_source_number }
            {
            }
            ~client_base( ) override = default;

            virtual void avail_info( detail::avail_info& info ) = 0;

            void
            set_source_number( int number ) noexcept
            {
                source_number_ = number;
            }
            int
            source_number( ) const noexcept
            {
                return source_number_;
            }

            virtual void get_16th_data_core( const data_plan&  plan,
                                             const time_point& last_time_seen,
                                             void*             buffer,
                                             std::size_t       buffer_size,
                                             data_status&      status_buf ) const  = 0;

            virtual void get_sec_data_core( const data_plan& plan,
                                            std::int64_t     start_second,
                                            std::size_t      stride,
                                            void*            buffer,
                                            std::size_t      buffer_size,
                                            sec_data_status& status_buf,
                                            uint64_t*        gps_loaded ) const = 0;

        private:
            int source_number_{ 0 };
        };

    } // namespace detail
} // namespace daqd_stream

#endif // DADQD_STREAM_PRIVATE_CLIENT_BASE_HH
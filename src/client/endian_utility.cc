#include "endian_utility.hh"

#include <cstdint>
#include <boost/endian/conversion.hpp>

namespace
{

    boost::endian::order
    to_boost_enum( daqd_stream::byte_order order )
    {
        switch ( order )
        {
        default:
        case daqd_stream::byte_order::NATIVE:
            return boost::endian::order::native;
        case daqd_stream::byte_order::BIG:
            return boost::endian::order::big;
        case daqd_stream::byte_order::LITTLE:
            return boost::endian::order::little;
        }
    }

    template < typename T >
    void
    flip( char* start, const char* end )
    {
        for ( auto cur = start; cur < end; cur += sizeof( T ) )
        {
            *reinterpret_cast< T* >( cur ) =
                boost::endian::endian_reverse( *reinterpret_cast< T* >( cur ) );
        }
    }
} // namespace

namespace daqd_stream
{
    namespace detail
    {
        void
        endian_convert_in_place( void*       buffer,
                                 std::size_t buffer_size,
                                 const std::vector< output_location >& metadata,
                                 std::size_t                           cycles,
                                 byte_order                            target )
        {
            auto order = to_boost_enum( target );
            if ( order == boost::endian::order::native )
            {
                return;
            }

            for ( const auto& chan_segment : metadata )
            {
                char* start = reinterpret_cast< char* >( buffer ) +
                    chan_segment.data_offset;
                auto end = start + ( chan_segment.bytes_per_16th * cycles );
                switch ( chan_segment.datatype )
                {
                case DATA_TYPE::INT16:
                    flip< std::int16_t >( start, end );
                    break;

                case DATA_TYPE::INT32:
                case DATA_TYPE::FLOAT32:
                case DATA_TYPE::UINT32:
                    flip< std::int32_t >( start, end );
                    break;

                case DATA_TYPE::INT64:
                case DATA_TYPE::FLOAT64:
                    flip< std::int64_t >( start, end );
                }
            }
        }
    } // namespace detail
} // namespace daqd_stream
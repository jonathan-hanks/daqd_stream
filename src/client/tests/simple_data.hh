//
// Created by jonathan.hanks on 2/7/22.
//

#ifndef DAQD_STREAM_SIMPLE_DATA_HH
#define DAQD_STREAM_SIMPLE_DATA_HH

#include <memory>

#include <cds-pubsub/pub.hh>

#include "frame_utils.hh"

pub_sub::Message create_metadata_blob(
    const std::vector< frame_utils::SimpleGenerator >& generators,
    unsigned int                                       dcuid );

pub_sub::Message
create_data_blob( const std::vector< frame_utils::SimpleGenerator >& generators,
                  unsigned int                                       dcuid,
                  std::int64_t                                       gps,
                  int                                                cycle );

#endif // DAQD_STREAM_SIMPLE_DATA_HH

//
// Created by jonathan.hanks on 11/9/21.
//
#include "sample_channel_lists.hh"

#include "data_types.hh"
#include "predicates.hh"

namespace
{
    daqd_stream::online_channel
    online( const char*            name,
            unsigned char          dcuid,
            daqd_stream::DATA_TYPE datatype,
            int                    datarate )
    {
        return daqd_stream::online_channel{
            name, 0, dcuid, datatype, datarate, 0, 0, 1.0, 1.0, 0.0, "counts",
        };
    }

    void
    sort( std::vector< daqd_stream::online_channel >& data )
    {
        std::sort(
            data.begin( ), data.end( ), daqd_stream::detail::less_by_name );
    }

    void
    update_offsets( std::vector< daqd_stream::online_channel >& channels )
    {
        std::size_t cur_offset = 0;
        for ( auto& cur_chan : channels )
        {
            cur_chan.data_offset = cur_offset;
            cur_chan.bytes_per_16th =
                daqd_stream::detail::data_type_size( cur_chan.datatype ) *
                cur_chan.datarate / 16;
            cur_offset += cur_chan.bytes_per_16th;
        }
    }

    void
    finalize_chan_list( std::vector< daqd_stream::online_channel >& channels )
    {
        update_offsets( channels );
        sort( channels );
    }
} // namespace

extern std::vector< daqd_stream::online_channel >
sample_channel_list_1( )
{
    using namespace daqd_stream;
    std::vector< online_channel > results{ };
    results.emplace_back(
        online( "sample_chan_5_1", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_2", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_3", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_4", 5, DATA_TYPE::FLOAT32, 16 ) );
    finalize_chan_list( results );
    return results;
}

extern std::vector< daqd_stream::online_channel >
sample_channel_list_1a( )
{
    using namespace daqd_stream;
    std::vector< online_channel > results{ };
    results.emplace_back(
        online( "sample_chan_5_1", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_2", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_4", 5, DATA_TYPE::FLOAT32, 16 ) );
    results.emplace_back(
        online( "sample_chan_5_3", 5, DATA_TYPE::FLOAT32, 256 ) );
    finalize_chan_list( results );
    return results;
}

extern std::vector< daqd_stream::online_channel >
sample_channel_list_1b( )
{
    using namespace daqd_stream;
    std::vector< online_channel > results{ };
    results.emplace_back(
        online( "sample_chan_5_1", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_2", 5, DATA_TYPE::FLOAT32, 256 ) );
    results.emplace_back(
        online( "sample_chan_5_4a", 5, DATA_TYPE::FLOAT32, 16 ) );
    results.emplace_back(
        online( "sample_chan_5_3", 5, DATA_TYPE::FLOAT32, 256 ) );
    finalize_chan_list( results );
    return results;
}

extern std::vector< daqd_stream::online_channel >
sample_channel_list_2( )
{
    using namespace daqd_stream;
    std::vector< online_channel > results{ };
    results.emplace_back(
        online( "sample_chan_6_1", 6, DATA_TYPE::FLOAT32, 16 ) );
    results.emplace_back(
        online( "sample_chan_6_2", 6, DATA_TYPE::FLOAT32, 16 ) );
    results.emplace_back(
        online( "sample_chan_6_3", 6, DATA_TYPE::FLOAT32, 16 ) );
    results.emplace_back(
        online( "sample_chan_6_4", 6, DATA_TYPE::FLOAT32, 16 ) );

    finalize_chan_list( results );
    return results;
}

//
// Created by jonathan.hanks on 11/19/21.
//
#include "catch.hpp"

#include "internal.hh"

std::ostream&
operator<<( std::ostream& os, const daqd_stream::detail::avail_info& info )
{
    os << "[" << info.start_gps( ) << ":" << info.start_cycle( ) << "-"
       << info.end_gps( ) << ":" << info.end_cycle( ) << "]";
    return os;
}

TEST_CASE( "Test avail_info overlaps" )
{
    using avail = daqd_stream::detail::avail_info;
    struct test_case_t
    {
        avail a1, a2;
        bool  result;
    };
    auto create_avail = []( std::uint64_t gps,
                            std::uint64_t cycle,
                            std::uint64_t buffer ) -> avail {
        return avail{ gps, cycle, buffer };
    };
    test_case_t test_cases[] = {
        test_case_t{ create_avail( 5, 0, 5 ), create_avail( 4, 0, 4 ), true },
        test_case_t{ create_avail( 1999999999, 0, 1999999999 ),
                     create_avail( 1000000001, 0, 1 ),
                     true },
        test_case_t{ create_avail( 1000000001, 0, 1 ),
                     create_avail( 1999999999, 0, 1999999999 ),
                     true },
        test_case_t{ create_avail( 1000000001, 0, 1 ),
                     create_avail( 1999999999, 0, 999999998 ),
                     false },
        test_case_t{ create_avail( 1999999999, 0, 999999998 ),
                     create_avail( 1000000001, 0, 1 ),
                     false },
        test_case_t{ create_avail( 1000000001, 0, 1 ),
                     create_avail( 1000000001, 0, 1 ),
                     true },
        // test_case_t{create_avail(1000000000, 1000000005),
        // create_avail(1000000003, 1000000006), true},
        // test_case_t{create_avail(1000000003, 1000000006),
        // create_avail(1000000000, 1000000005), true},
    };
    for ( const auto& test_case : test_cases )
    {
        auto actual =
            daqd_stream::detail::overlaps( test_case.a1, test_case.a2 );
        std::cout << "test overlap of " << test_case.a1 << " " << test_case.a2
                  << " expect " << test_case.result << " got " << actual
                  << "\n";
        REQUIRE( actual == test_case.result );
    }
}

TEST_CASE( "Test avail_info intersection" )
{
    using avail = daqd_stream::detail::avail_info;
    struct test_case_t
    {
        avail a1, a2;
        avail result;
    };
    auto create_avail = []( std::uint64_t gps,
                            std::uint64_t cycle,
                            std::uint64_t buffer ) -> avail {
        return avail{ gps, cycle, buffer };
    };
    test_case_t test_cases[] = {
        test_case_t{ create_avail( 5, 0, 4 ),
                     create_avail( 6, 0, 2 ),
                     create_avail( 5, 0, 1 ) },
        test_case_t{ create_avail( 5, 0, 4 ),
                     create_avail( 5, 0, 2 ),
                     create_avail( 5, 0, 2 ) },
        test_case_t{ create_avail( 6, 6, 4 ),
                     create_avail( 6, 7, 3 ),
                     avail( 6, 6, 3, 7 ) },
    };
    for ( const auto& test_case : test_cases )
    {
        auto actual =
            daqd_stream::detail::intersection( test_case.a1, test_case.a2 );
        std::cout << "test intersection of " << test_case.a1 << " "
                  << test_case.a2 << " expect " << test_case.result << " got "
                  << actual << "\n";
        REQUIRE( actual == test_case.result );
    }
}
//
// Created by jonathan.hanks on 2/4/22.
//
#include "test_helpers.hh"

#include <limits>
#include <mutex>
#include <random>
#include <sstream>

#include <boost/filesystem.hpp>

namespace
{
    using lock_type = std::unique_lock< std::mutex >;
    static std::mutex&
    random_mutex( )
    {
        static std::mutex m{ };
        return m;
    }

    static auto
    random_seed( ) -> auto
    {
        static std::random_device r;
        lock_type                 l_{ random_mutex( ) };
        return r( );
    }

    template < typename T >
    struct dist_type_
    {
    };

    template <>
    struct dist_type_< int >
    {
        using type = std::uniform_int_distribution< int >;
    };

    template <>
    struct dist_type_< float >
    {
        using type = std::uniform_real_distribution< float >;
    };

    template <>
    struct dist_type_< double >
    {
        using type = std::uniform_real_distribution< double >;
    };

    class random_generator
    {
    public:
        random_generator( ) : engine_{ random_seed( ) }
        {
        }
        template < typename T >
        T
        uniform( T lower = std::numeric_limits< T >::min( ),
                 T upper = std::numeric_limits< T >::max( ) )
        {
            typename dist_type_< T >::type dist( lower, upper );
            return dist( engine_ );
        }

    private:
        std::default_random_engine engine_;
    };
} // namespace

namespace helpers
{
    std::string
    random_shm_name( )
    {
        random_generator   r;
        std::ostringstream os;
        os << "stream_" << r.uniform< int >( 0, 100000 );
        return os.str( );
    }

    std::function< void( void ) >
    cleanup_shmem_action( const std::vector< std::string >& names )
    {
        return [ names ]( ) -> void {
            for ( const auto& name : names )
            {
                if ( name.find( ".." ) != std::string::npos ||
                     name.find( '/' ) != std::string::npos )
                {
                    continue;
                }
                boost::system::error_code ec{ };
                boost::filesystem::remove( "/dev/shm/" + name, ec );
            }
        };
    }
} // namespace helpers

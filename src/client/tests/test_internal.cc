//
// Created by jonathan.hanks on 2/18/21.
//
#include "client_internal.hh"
#include <daqd_stream/daqd_stream.hh>
#include "catch.hpp"

#include <iostream>
#include <memory>
#include <vector>

#include "sample_channel_lists.hh"

// TEST_CASE( "Test static constants" )
//{
//    REQUIRE( daqd_stream::detail::safe_data_seconds( ) ==
//             daqd_stream::detail::total_data_seconds( ) - 1 );
//}

TEST_CASE( "test time_point_from_cycle" )
{
    struct Test_case
    {
        std::uint64_t           second;
        std::uint64_t           cycle;
        daqd_stream::time_point expected;
    };
    std::vector< Test_case > test_cases{
        { 42,
          5,
          daqd_stream::make_time_point(
              42, 5 * daqd_stream::detail::nano_in_cycle( ) ) },
        { 1,
          0,
          daqd_stream::make_time_point(
              1, 0 * daqd_stream::detail::nano_in_cycle( ) ) },
    };

    for ( const auto& test_case : test_cases )
    {
        auto actual = daqd_stream::detail::time_point_from_cycle(
            test_case.second, test_case.cycle );
        REQUIRE( actual.seconds == test_case.expected.seconds );
        REQUIRE( actual.nano == test_case.expected.nano );
    }
}

TEST_CASE( "test get_next_block_index_16th" )
{
    std::vector< char > buffer;
    buffer.resize( 1024 * 1024 * 1024 * 1 );
    auto* header = new ( reinterpret_cast< void* >( buffer.data( ) ) )
        daqd_stream::detail::shared_mem_header( buffer.size( ) );

    header->set_cur_block( 0 );

    std::int64_t start_gps = 1000000000 - 1;
    std::int64_t start_cycle = 15;
    auto         cur_gps = start_gps;
    auto         cur_cycle = start_cycle;

    // force a wrap around
    int total_blocks =
        header->max_data_blocks( ) + header->max_data_blocks( ) / 2;

    for ( auto i = 0; i < total_blocks; ++i )
    {
        header->advance_block_counter( );
        ++cur_cycle;
        if ( cur_cycle >= 16 )
        {
            cur_cycle = 0;
            ++cur_gps;
        }

        daqd_stream::detail::data_block& cur_block =
            header->blocks( header->get_cur_block( ) );

        cur_block.gps_second = cur_gps;
        cur_block.cycle = cur_cycle;
    }

    auto cur_index = header->get_cur_block( );

    struct Test_case
    {
        int                     cur_block_index;
        daqd_stream::time_point time;
        int                     expected_index;
    };

    auto time_from_cur_cycle =
        [ header ]( int steps_backwards ) -> daqd_stream::time_point {
        auto cur = header->get_cur_block( );
        for ( auto i = 0; i < steps_backwards; ++i )
        {
            cur = header->prev_block( cur );
        }
        return daqd_stream::detail::time_point_from_cycle(
            header->blocks( cur ).gps_second, header->blocks( cur ).cycle );
    };

    auto prev_index = [ header ]( int steps_backwards ) -> int {
        auto cur = header->get_cur_block( );
        for ( auto i = 0; i < steps_backwards; ++i )
        {
            cur = header->prev_block( cur );
        }
        return cur;
    };

    std::vector< Test_case > test_cases{
        {
            cur_index,
            daqd_stream::make_time_point( 0, 0 ),
            cur_index,
        },
        {
            cur_index,
            time_from_cur_cycle( 1 ),
            cur_index,
        },
        {
            cur_index,
            time_from_cur_cycle( 3 ),
            prev_index( 2 ),
        },
        {
            cur_index,
            time_from_cur_cycle( header->max_data_blocks( ) - 1 ),
            prev_index( header->safe_data_blocks( ) - 1 ),
        },
    };

    for ( const auto& test_case : test_cases )
    {
        header->set_cur_block( test_case.cur_block_index );
        auto actual_index = daqd_stream::detail::get_next_block_index_16th(
            header, test_case.time );
        REQUIRE( actual_index == test_case.expected_index );
    }
}

TEST_CASE( "test get_start_block_index_sec" )
{
    std::vector< char > buffer;
    buffer.resize( 1024 * 1024 * 1024 * 1 );
    auto* header = new ( reinterpret_cast< void* >( buffer.data( ) ) )
        daqd_stream::detail::shared_mem_header( buffer.size( ) );

    header->set_cur_block( 0 );

    std::int64_t start_gps = 1000000000 - 1;
    std::int64_t start_cycle = 15;
    auto         cur_gps = start_gps;
    auto         cur_cycle = start_cycle;

    // force a wrap around
    int total_blocks =
        header->max_data_blocks( ) + header->max_data_blocks( ) / 2 + 8;

    for ( auto i = 0; i < total_blocks; ++i )
    {
        header->advance_block_counter( );

        ++cur_cycle;
        if ( cur_cycle >= 16 )
        {
            cur_cycle = 0;
            ++cur_gps;
        }

        daqd_stream::detail::data_block& cur_block =
            header->blocks( header->get_cur_block( ) );

        cur_block.gps_second = cur_gps;
        cur_block.cycle = cur_cycle;
    }

    {
        int i = 0;
        for ( auto cur_block = header->blocks_begin( );
              cur_block < header->blocks_end( );
              ++cur_block )
        {
            auto& block = *cur_block;
            std::cout << i << " " << block.gps_second << " - " << block.cycle
                      << ( i == header->get_cur_block( ) ? " *" : "" ) << "\n";
            ++i;
        }
    }

    auto cur_index = header->get_cur_block( );

    struct Test_case
    {
        int          cur_block_index;
        std::int64_t start_time;
        std::int64_t expected_time;
    };

    auto find_index = [ header ]( std::int64_t req_sec ) -> int {
        auto index = 0;
        for ( auto cur_block = header->blocks_begin( );
              cur_block < header->blocks_end( );
              ++cur_block )
        {

            if ( cur_block->gps_second == req_sec && cur_block->cycle == 0 )
            {
                return index;
            }
            ++index;
        }
        throw std::runtime_error( "requested time not found" );
    };

    std::int64_t cur_sec = header->blocks( cur_index ).gps_second;

    std::vector< Test_case > test_cases{
        {
            cur_index,
            0,
            cur_sec,
        },
        {
            cur_index,
            cur_sec,
            cur_sec,
        },
        {
            cur_index,
            cur_sec - 1,
            cur_sec - 1,
        },
        {
            cur_index,
            cur_sec - 2,
            cur_sec - 2,
        },
        {
            cur_index,
            cur_sec - 3,
            cur_sec - 3,
        },
        {
            cur_index,
            cur_sec - 4,
            cur_sec - 3,
        },
        { cur_index, cur_sec - 5, cur_sec - 3 }
    };

    for ( const auto& test_case : test_cases )
    {
        header->set_cur_block( test_case.cur_block_index );
        auto actual_index = daqd_stream::detail::get_start_block_index_sec(
            header, test_case.start_time );
        auto actual_time = header->blocks( actual_index ).gps_second;
        std::cout << "time: " << test_case.start_time << "\n";
        std::cout << "\texpected " << test_case.expected_time << "\n";
        std::cout << "\tgot      " << header->blocks( actual_index ).gps_second
                  << "-" << header->blocks( actual_index ).cycle << actual_index
                  << "\n";
        REQUIRE( actual_time == test_case.expected_time );
        // for this test setup we should always end up on cycle 0
        REQUIRE( header->blocks( actual_index ).cycle == 0 );
    }
}

TEST_CASE( "Test find_by_name with std::vector" )
{
    unsigned char src{ 0 };
    auto          channels = sample_channel_list_1( );

    for ( const auto& search_for : channels )
    {
        src = 1;
        std::string search_name( search_for.name.data( ) );
        auto        it = daqd_stream::detail::find_by_name_with_source(
            channels, search_name, src );
        REQUIRE( it != channels.end( ) );
        REQUIRE( it->name == search_name.c_str( ) );
        REQUIRE( src == 0 );
    }
    {
        std::string non_existant{ "non existant" };
        auto        it = daqd_stream::detail::find_by_name_with_source(
            channels, non_existant, src );
        REQUIRE( it == channels.end( ) );
    }
}

TEST_CASE( "Test find_by_name with a shared span" )
{
    unsigned char src{ 0 };
    auto          channels_vec = sample_channel_list_1( );
    daqd_stream::detail::shared_span< daqd_stream::online_channel > channels(
        channels_vec.data( ), channels_vec.size( ) );

    for ( const auto& search_for : channels )
    {
        src = 1;
        std::string search_name( search_for.name.data( ) );
        auto        it = daqd_stream::detail::find_by_name_with_source(
            channels, search_name, src );
        REQUIRE( it != channels.end( ) );
        REQUIRE( it->name == search_name.c_str( ) );
        REQUIRE( src == 0 );
    }
    {
        std::string non_existant{ "non existant" };
        auto        it = daqd_stream::detail::find_by_name_with_source(
            channels, non_existant, src );
        REQUIRE( it == channels.end( ) );
    }
}

TEST_CASE( "Test find_by_name with a multi segment" )
{
    auto channels_vec1 = sample_channel_list_1( );
    auto channels_vec2 = sample_channel_list_2( );
    //    daqd_stream::detail::shared_span< daqd_stream::online_channel >
    //    channels_span1(
    //        channels_vec1.data( ), channels_vec1.size( ) );
    //    daqd_stream::detail::shared_span< daqd_stream::online_channel >
    //    channels_span2(
    //        channels_vec2.data( ), channels_vec2.size( ) );

    unsigned char                           src{ 0 };
    daqd_stream::detail::multi_channel_list ml;
    REQUIRE( daqd_stream::detail::find_by_name_with_source(
                 ml, "sample_chan_6_1", src ) == nullptr );

    ml.add_source( channels_vec1.data( ), channels_vec1.size( ) );
    REQUIRE( daqd_stream::detail::find_by_name_with_source(
                 ml, "sample_chan_6_1", src ) == nullptr );

    src = 0;
    ml.add_source( channels_vec2.data( ), channels_vec2.size( ) );
    REQUIRE( daqd_stream::detail::find_by_name_with_source(
                 ml, "sample_chan_6_1", src ) != nullptr );
    REQUIRE( src == 1 );

    for ( const auto& search_for : channels_vec1 )
    {
        src = 5;
        std::string search_name( search_for.name.data( ) );
        auto        it = daqd_stream::detail::find_by_name_with_source(
            ml, search_name, src );
        REQUIRE( it != nullptr );
        REQUIRE( it->name == search_name.c_str( ) );
        REQUIRE( src == 0 );
    }
    for ( const auto& search_for : channels_vec2 )
    {
        src = 5;
        std::string search_name( search_for.name.data( ) );
        auto        it = daqd_stream::detail::find_by_name_with_source(
            ml, search_name, src );
        REQUIRE( it != nullptr );
        REQUIRE( it->name == search_name.c_str( ) );
        REQUIRE( src == 1 );
    }
    {
        std::string non_existant{ "non existant" };
        auto        it = daqd_stream::detail::find_by_name_with_source(
            ml, non_existant, src );
        REQUIRE( it == ml.end( ) );
        REQUIRE( it == nullptr );
    }
}
import contextlib
import integration
import os.path
import time
import threading

import numpy
import requests

import daqd_stream

integration.Executable(name="daqd_stream_srv", hints=['../apps/daqd_stream_server'], description="daqd stream server")
integration.Executable(name="fe_simulated_streams",
                       hints=["/usr/bin", ],
                       description="data server")
integration.Executable(name="cps_xmit",
                       hints=["/usr/bin", ],
                       description="data xmit")
integration.Executable(name="cds_metadata_server", hints=["/usr/bin"], description="channel metadata server")
integration.Executable(name="test_daqd_dcu_status_progressions", hints=[], description="test program")

# flag file directory
# valid flags are "receiving_data", "mod5_missing", "mod5_restored" "client_aborted"
flag_dir = integration.state.temp_dir("event_flags")

ini_dir = integration.state.temp_dir('inis')
master_file = os.path.join(ini_dir, 'master')

generator = integration.Process("fe_simulated_streams",
                                ["-b", "local_dc",
                                 "-m", "100",
                                 "-k", "100",
                                 "-R", "5",
                                 "-i", ini_dir,
                                 "-M", master_file,
                                 "-admin", "127.0.0.1:10000",
                                 "-s", "shm://",
                                 ])
xmit = integration.Process("cps_xmit",
                           ["-b", "shm://local_dc",
                            "-m", "100",
                            "-p", "tcp://127.0.0.1:9000"],)
metadata = integration.Process("cds_metadata_server",
                               ["-listen", "127.0.0.1:9100",
                                "-master", master_file, ])
server = integration.Process("daqd_stream_srv", [
    "-d", "tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9100"
])
test_program = integration.Process("test_daqd_dcu_status_progressions",
                                   ["--flag-dir", flag_dir,
                                    "--status-codes", "0,bad,0", ])


def fail():
    raise RuntimeError("Fail here")


@contextlib.contextmanager
def cleanup_shmem(segments=('local_dc', 'daqd_stream',)):
    yield
    for entry in segments:
        path = os.path.join('/dev/shm', entry)
        try:
            os.remove(path)
        except:
            pass


channel_lists = {}


def get_channels(dest):
    def wrapper():
        global channel_lists

        client = daqd_stream.client("daqd_stream")
        channel_lists[dest] = client.channels()
        del client

    return wrapper


def do_simple_dcuid_request(action:str, dcuid):
    resp = requests.post("http://localhost:10000/api/v1/dcu/{0}/{1}/".format(dcuid, action))
    if resp.status_code != requests.codes.ok:
        raise RuntimeError("Unexpected return from the {2} request {0}: {1}".format(resp.status_code, resp.text, action))


def stop_dcuid(dcuid):
    def do_stop():
        do_simple_dcuid_request("stop", dcuid)

    return do_stop


def start_dcuid(dcuid):
    def do_start():
        do_simple_dcuid_request("start", dcuid)

    return do_start


def mutate_dcuid(dcuid):
    def do_mutate():
        resp = requests.post("http://localhost:10000/api/v1/dcu/{0}/mutate/".format(dcuid),
                             data={'DcuId': dcuid, 'RemoveCount': 100, 'AddCount': 150,
                                   'RemoveChannels': '', 'AddChannels': '', 'ProtectedChannels': ''})
        if resp.status_code != requests.codes.ok:
            raise RuntimeError("Unexpected return from the mutate request {0}: {1}".format(resp.status_code, resp.text))
        time.sleep(1.0)
        do_simple_dcuid_request("start", dcuid)
    return do_mutate


def is_flag_set(flag:str):
    global flag_dir
    return os.path.exists(os.path.join(flag_dir, flag))


def set_flag(flag:str):
    global flag_dir
    with open(os.path.join(flag_dir, flag), "wb") as f:
        f.write(b"\n")


def wait(delay=3.0):
    return integration.callable(time.sleep, delay)


def wait_event(name, timeout=5000.0):
    def do_wait_event():

        if is_flag_set("client_aborted"):
            raise RuntimeError("client aborted")
        if is_flag_set(name):
            return
        max_iterations = int(timeout * 10)
        for i in range(max_iterations):
            if is_flag_set("client_aborted"):
                raise RuntimeError("client aborted")
            if is_flag_set(name):
                return
            time.sleep(0.1)
        raise RuntimeError("wait on {0} timed out".format(name))
    return do_wait_event


def echo(msg:str):
    def do_echo():
        print(msg)
    return do_echo


# set_flag("pause")

with cleanup_shmem():
    integration.Sequence(
        [
            # integration.state.preserve_files,
            generator.run,
            integration.wait_tcp_server(port=10000, timeout=5),
            xmit.run,
            integration.wait_tcp_server(port=9000, timeout=5),
            metadata.run,
            integration.wait_tcp_server(port=9100, timeout=5),
            server.run,
            wait(1.0),
            echo("starting test program"),
            test_program.run,
            echo("test program started"),
            wait_event('mod5_0_0'),
            echo("test program signaled that it received data"),
            stop_dcuid(5),
            wait_event('mod5_1_bad'),
            test_program.ignore,
            start_dcuid(5),
            wait_event('mod5_2_0'),
            server.ignore,
            metadata.ignore,
            xmit.ignore,
            generator.ignore,
         ]
    )
print("done!")

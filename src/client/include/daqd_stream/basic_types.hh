/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_BASIC_TYPES_HH
#define DAQD_STREAM_BASIC_TYPES_HH

#include <cstdlib>
#include <cstdint>

namespace daqd_stream
{
    using checksum_t = std::uint32_t;

    enum class byte_order
    {
        NATIVE,
        LITTLE,
        BIG,
    };

    /**
     * @brief The status of the plan, whether it is ok/current or needs updating
     * @noop tag PLAN_STATUS
     */
    enum class PLAN_STATUS
    {
        OK,
        UPDATE_PLAN,
    };

    /*!
     * @brief the set of supported data types.
     * @note COMPLEX32 is two 32bit floats per entry.
     * @noop tag DATA_TYPE
     */
    enum class DATA_TYPE : short
    {
        UNDEFINED = 0,
        INT16 = 1,
        INT32 = 2,
        INT64 = 3,
        FLOAT32 = 4,
        FLOAT64 = 5,
        COMPLEX32 = 6,
        UINT32 = 7
    };

} // namespace daqd_stream

#endif // DAQD_STREAM_BASIC_TYPES_HH

/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include <daqd_stream/daqd_stream_server.hh>
#include <daqd_stream/daqd_stream.hh>

#include <sstream>

#include "daqd_stream_intl.hh"

namespace daqd_stream
{
    namespace
    {
        std::string
        create_conn_str( const std::string& metadata_source,
                         const std::string& data_source )
        {
            std::ostringstream os;
            os << data_source << "|meta://" << metadata_source;
            return os.str( );
        }
    }; // namespace

    daqd_stream::daqd_stream( const std::string& data_source,
                              const std::string& shmem_name,
                              buffer_parameters  parameters,
                              plugin_list        additional_plugins )
        : p_{ new detail::daqd_stream_intl(
              data_source, shmem_name, parameters, additional_plugins ) }
    {
    }

    daqd_stream::daqd_stream( const std::string& shmem_name,
                              const std::vector< source_parameters >& sources,
                              plugin_list additional_plugins )
        : p_{ new detail::daqd_stream_intl(
              shmem_name, sources, additional_plugins ) }
    {
    }

    daqd_stream::~daqd_stream( ) = default;

    void
    daqd_stream::run( )
    {
        p_->run( );
    }

    void
    daqd_stream::stop( )
    {
        p_->stop( );
    }

    std::unique_ptr< client >
    daqd_stream::get_client( )
    {
        return client::create( p_->data_source_str_ );
    }
} // namespace daqd_stream
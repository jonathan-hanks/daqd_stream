//
// Created by jonathan.hanks on 2/3/22.
//
#include "catch.hpp"
#include "frame_utils.hh"
#include <atomic>
#include <thread>

#include "test_helpers.hh"

#include <daqd_stream/daqd_stream_server.hh>

void
wait_for_channels( daqd_stream::client& client )
{
    while ( client.channels( ).empty( ) )
    {
        std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
    }
}

std::vector< std::string >
pick_channels( const std::vector< daqd_stream::online_channel >& channels )
{
    std::vector< std::string > picks{ };
    for ( auto i = 0; i < 5; ++i )
    {
        picks.emplace_back( channels.at( i * 5 ).name.data( ) );
    }
    return picks;
}

TEST_CASE( "daqd_stream server can consume frames and serve its data" )
{
    std::atomic< bool >  done_{ false };
    frame_utils::TempDir tmp_dir{ };
    auto generators = frame_utils::create_sample_channels( 100 );
    frame_utils::frame_generator_config cfg{ };
    cfg.frame_root = tmp_dir.get( ).string( );
    cfg.verbose = false;
    frame_utils::frame_generator g( cfg, generators );

    g.generate( );

    std::string conn_str =
        std::string( "gwf://X1-TEST:" ) + tmp_dir.get( ).string( );
    daqd_stream::buffer_parameters params{ };
    params.seconds_in_buffer = 4;
    params.total_buffer_size = 500 * 1024 * 1024;

    auto buffer_name = helpers::random_shm_name( );

    daqd_stream::daqd_stream server( conn_str, buffer_name, params, { } );
    std::thread server_loop_thread( [ &server ]( ) { server.run( ); } );
    {
        helpers::cleanup_action stop( [ &server ]( ) { server.stop( ); } );
        helpers::cleanup_action rm_shm(
            helpers::cleanup_shmem_action( { buffer_name } ) );
        std::cerr << "about to get the client\n";
        auto client = daqd_stream::client::create( buffer_name );
        wait_for_channels( *client );
        std::cerr << "got " << client->channels( ).size( ) << " channels\n";

        auto channel_names = pick_channels( client->channels( ) );

        auto plan = client->plan_request(
            daqd_stream::PLAN_TYPE::PLAN_16TH, channel_names, 0 );
        auto plan_sec = client->plan_request(
            daqd_stream::PLAN_TYPE::PLAN_SEC, channel_names, 1 );
        std::vector< char > buffer( plan.required_size( ) );
        std::vector< char > buffer_sec( plan_sec.required_size( ) );

        daqd_stream::time_point      last_seen{ };
        daqd_stream::data_status     status{ };
        daqd_stream::sec_data_status status_sec{ };
        // std::cerr << "about to get data\n";

        auto expected_gps = 1000000000;
        auto expected_cycle = 15;

        std::uint64_t sec_requested = 0;

        for ( auto i = 0; i < 100; ++i )
        {
            client->get_16th_data(
                plan, last_seen, buffer.data( ), buffer.size( ), status );
            //            std::cerr << "got data: " << status.data_good << " "
            //                      << status.gps.seconds << ":" << status.cycle
            //                      << "\n";
            last_seen = status.gps;
            REQUIRE( status.data_good );
            REQUIRE( status.gps.seconds == expected_gps );
            REQUIRE( status.cycle == expected_cycle );
            if ( status.cycle == 15 )
            {
                client->get_sec_data( plan_sec,
                                      sec_requested,
                                      1,
                                      buffer_sec.data( ),
                                      buffer_sec.size( ),
                                      status_sec );
                REQUIRE( status_sec.gps.seconds == expected_gps );
                g.generate_corrupt( );
                std::this_thread::sleep_for( std::chrono::milliseconds (750) );
                g.generate();
            }
            expected_cycle = ( expected_cycle + 1 ) % 16;
            if ( expected_cycle == 0 )
            {
                ++expected_gps;
            }
        }
    }
    server_loop_thread.join( );
}
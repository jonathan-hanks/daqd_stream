//
// Created by jonathan.hanks on 10/30/21.
//
#include <catch.hpp>

#include "msg_to_ifo_config.hh"

#include <cstring>
#include <iterator>
#include <map>
#include <set>

namespace
{
    struct MessageBlob
    {
        using value_type = pub_sub::Message;

        value_type
        allocate( std::size_t size )
        {
            auto ptr = std::shared_ptr< unsigned char[] >(
                new unsigned char[ size ], []( unsigned char* p ) {
                    if ( p )
                    {
                        delete[] p;
                    }
                } );
            return pub_sub::Message( std::move( ptr ), size );
        }

        void*
        data_pointer( const value_type& blob )
        {
            return blob.data( );
        }
    };

    daq_channel_metadata_t
    identity( const daq_channel_metadata_t& entry )
    {
        return entry;
    };
} // namespace

TEST_CASE(
    "We should be able to see all the channels and DCUs in a ifo_config" )
{
    daq_metadata::MetadataBuilder builder;

    const auto channels5 = {
        daq_channel_metadata_t{ "mod5-1", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod5-10", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{ "mod5-2", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod5-20", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
    };
    const auto channels6 = {
        daq_channel_metadata_t{ "mod6-1", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{ "mod6-2", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod6-20", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{ "mod6-3", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
    };
    const auto channels11 = {
        daq_channel_metadata_t{
            "mod11-11", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod11-1", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod11-10", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
        daq_channel_metadata_t{
            "mod11-4", "counts", 2, 2048, 0, 1.0, 1.0, 0.0 },
    };

    builder.append( 5, 42, channels5, identity );
    builder.append( 6, 43, channels6, identity );
    builder.append( 11, 44, channels11, identity );

    auto msg = builder.to_binary_blob( MessageBlob( ) );

    daqd_stream::detail::ifo_config cfg{ };
    REQUIRE( daqd_stream::detail::msg_to_ifo_config( msg, &cfg ) );

    std::vector< daq_channel_metadata_t > expected{ };
    std::copy(
        channels5.begin( ), channels5.end( ), std::back_inserter( expected ) );
    std::copy(
        channels6.begin( ), channels6.end( ), std::back_inserter( expected ) );
    std::copy( channels11.begin( ),
               channels11.end( ),
               std::back_inserter( expected ) );

    for ( const auto& exp_chan : expected )
    {
        auto it = std::find_if(
            cfg.channels.begin( ),
            cfg.channels.end( ),
            [ &exp_chan ]( const daqd_stream::online_channel& chan ) -> bool {
                return chan.name == exp_chan.name;
            } );
        REQUIRE( it != cfg.channels.end( ) );
    }

    //    for ( const auto& exp_chan : expected )
    //    {
    //        auto it = cfg.find_channel( exp_chan.name );
    //        if ( it == cfg.channels.end( ) )
    //        {
    //            std::cout << "lookup failed for " << exp_chan.name << "\n";
    ////            bool expected_to_find =
    ////                std::binary_search( cfg.channels.begin( ),
    ////                                    cfg.channels.end( ),
    ////                                    exp_chan.name,
    ////                                    daqd_stream::detail::search_by_name(
    ///) ); /            std::cout << "expected to find value = " <<
    /// expected_to_find /                      << "\n"; /            auto
    /// expected_linear = /                ( std::find_if( / cfg.channels.begin(
    ///), /                      cfg.channels.end( ), /                      [
    /// exp_chan ]( /                          const
    /// daqd_stream::online_channel& chan ) -> bool { / return chan.name ==
    /// exp_chan.name; /                      } ) != cfg.channels.end( ) ); /
    /// std::cout << "expected to find linear value = " << expected_linear / <<
    ///"\n";
    //            std::cout << "channel list is: \n";
    //            daqd_stream::detail::search_by_name searcher;
    //            for ( const auto& ch : cfg.channels )
    //            {
    //                auto ch_name = ch.name.data( );
    //                auto search_name = exp_chan.name;
    //                auto search_result = searcher( search_name, ch );
    //                std::cout << "\t'" << ch_name << "' " << search_result <<
    //                "\n";
    //            }
    //        }
    //        REQUIRE( it != cfg.channels.end( ) );
    //    }
}
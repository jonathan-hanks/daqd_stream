#ifndef DAQD_STREAM_REMOTE_METADATA_HH
#define DAQD_STREAM_REMOTE_METADATA_HH

#include "meta_data.hh"

#include <daqd_stream/daqd_stream.hh>

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief Parse an ini file
         * @param hostname The path to the master file
         * @return
         */
        ifo_config retrieve_remote_meta_data( const std::string& hostname,
                                              int                port = 9010 );

        bool retrieve_remote_meta_data( const std::string& hostname,
                                        int                port,
                                        dcu_metadata_cb    per_dcu_callback );
    } // namespace detail
} // namespace daqd_stream

#endif //  DAQD_STREAM_REMOTE_METADATA_HH
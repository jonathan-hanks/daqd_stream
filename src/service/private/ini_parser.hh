/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#ifndef DAQD_STREAM_INI_PARSER_HH
#define DAQD_STREAM_INI_PARSER_HH

#include "meta_data.hh"

namespace daqd_stream
{
    namespace detail
    {

        /*!
         * @brief Parse an ini file
         * @param filename The path to the master file
         * @return
         */
        ifo_config parse_master_file( const std::string& filename );
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_INI_PARSER_HH

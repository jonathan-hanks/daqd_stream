//
// Created by jonathan.hanks on 10/30/21.
//

#ifndef DAQD_STREAM_MSG_TO_IFO_CONFIG_HH
#define DAQD_STREAM_MSG_TO_IFO_CONFIG_HH

#include <algorithm>
#include <utility>

#include <cds-pubsub/sub.hh>
#include <advligorts/daq_metadata.h>
#include <advligorts/daq_metadata_utils.hh>

#include "internal.hh"
#include "meta_data.hh"

namespace daqd_stream
{
    namespace detail
    {
        inline bool
        msg_to_ifo_config( pub_sub::Message& msg, ifo_config* dest_config )
        {
            ifo_config new_config{ };
            if ( msg.data( ) == nullptr || msg.length == 0 || !dest_config )
            {
                std::cerr << "metadata msg or dest null\n";
                return false;
            }
            auto* metadata_header =
                reinterpret_cast< daq_multi_dcu_metadata_t* >( msg.data( ) );
            if ( !daq_metadata::verify( metadata_header, msg.length ) )
            {
                std::cerr << "metadata verify failed\n";
                return false;
            }
            unsigned int numChannels = 0;
            if ( !daq_metadata::visit_each_dcu(
                     metadata_header,
                     [ &numChannels ]( const daq_dcu_metadata_header_t& header,
                                       const void* data ) -> bool {
                         numChannels += header.numChannels;
                         return true;
                     } ) )
            {
                return false;
            }

            new_config.channels.reserve( numChannels );

            daq_metadata::visit_each_dcu(
                metadata_header,
                daq_metadata::unpack(
                    [ &new_config ](
                        const daq_dcu_metadata_header_t& header,
                        const daq_channel_metadata_t*    begin,
                        const daq_channel_metadata_t*    end ) -> bool {
                        new_config.checksums.dcu_config[ header.dcuId ] =
                            header.checksum;
                        std::size_t cur_offset = 0;
                        std::transform(
                            begin,
                            end,
                            std::back_inserter( new_config.channels ),
                            [ &header, &cur_offset ](
                                const daq_channel_metadata_t& input )
                                -> online_channel {
                                online_channel dest{ };
                                dest.name = input.name;
                                dest.dcuid =
                                    static_cast< short >( header.dcuId );
                                dest.datatype =
                                    static_cast< DATA_TYPE >( input.datatype );
                                dest.datarate = input.datarate;
                                dest.bytes_per_16th = calculate_bytes_per_16th(
                                    dest.datatype, input.datarate );
                                dest.data_offset = cur_offset;
                                dest.signal_gain = input.signal_gain;
                                dest.signal_slope = input.signal_slope;
                                dest.signal_offset = input.signal_offset;
                                dest.units = input.units;

                                cur_offset += dest.bytes_per_16th;
                                return dest;
                            } );
                        return true;
                    } ) );

            new_config.checksums.recalculate_master_config( );
            std::sort( new_config.channels.begin( ),
                       new_config.channels.end( ),
                       less_by_name );

            std::swap( new_config, *dest_config );
            return true;
        }

    } // namespace detail

} // namespace daqd_stream

#endif // DAQD_STREAM_MSG_TO_IFO_CONFIG_HH

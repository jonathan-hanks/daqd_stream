/*
Copyright 2022 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/

#ifndef DAQD_STREAM_DAQD_STREAM_SERVER_HH
#define DAQD_STREAM_DAQD_STREAM_SERVER_HH

#include <memory>
#include <string>
#include <vector>

#include <cds-pubsub/sub_plugin.hh>

namespace daqd_stream
{
    class client;

    namespace detail
    {
        struct daqd_stream_intl;
    }

    class buffer_parameters
    {
    public:
        explicit buffer_parameters(
            int         seconds_in_buffer_ = 5,
            std::size_t total_buffer_size_ = ( 1024 * 1024 * 1024 ) +
                ( 1024 * 1024 * 800 ),
            int permissions_ = 0640 )
            : seconds_in_buffer{ seconds_in_buffer_ },
              total_buffer_size{ total_buffer_size_ }, permissions{
                  permissions_
              }
        {
        }

        int         seconds_in_buffer;
        std::size_t total_buffer_size;
        int         permissions;
    };

    class source_parameters
    {
    public:
        source_parameters( std::string       conn_source,
                           std::string       conn_label,
                           buffer_parameters buffer_params )
            : source_{ std::move( conn_source ) },
              label_{ std::move( conn_label ) }, parameters_{ buffer_params }
        {
        }

        source_parameters( const source_parameters& ) = default;
        source_parameters( source_parameters&& ) = default;
        source_parameters& operator=( const source_parameters& ) = default;
        source_parameters& operator=( source_parameters&& ) = default;

        const std::string&
        source( ) const noexcept
        {
            return source_;
        }
        const std::string&
        label( ) const noexcept
        {
            return label_;
        }

        const buffer_parameters&
        parameters( ) const noexcept
        {
            return parameters_;
        }

    private:
        std::string       source_;
        std::string       label_;
        buffer_parameters parameters_;
    };

    /*!
     * @brief a 'Server' thread which creates the shared interface for clients
     * to read data and metadata.
     */
    class daqd_stream
    {
    public:
        using plugin_list = std::vector<
            std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi > >;

        explicit daqd_stream(
            const std::string& data_source =
                "tcp://127.0.0.1:9000|meta://cds://127.0.0.1:9010",
            const std::string& shmem_name = "daqd_stream",
            buffer_parameters  parameters = buffer_parameters( ),
            plugin_list        additional_plugins = plugin_list( ) );
        daqd_stream( const std::string&                      shmem_name,
                     const std::vector< source_parameters >& sources,
                     plugin_list additional_plugins );
        ~daqd_stream( );

        std::unique_ptr< client > get_client( );

        void run( );
        void stop( );

    private:
        std::unique_ptr< detail::daqd_stream_intl > p_;
    };
} // namespace daqd_stream

#endif // DAQD_STREAM_DAQD_STREAM_SERVER_HH

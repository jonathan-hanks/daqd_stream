if(NDS_CLIENT_INCLUDE_DIRS)
    message("building cross check test")
add_executable(daqd_stream_cross_check
        main.cc
        daqd_stream.cc
        nds_stream.cc)
target_link_libraries(daqd_stream_cross_check PRIVATE
        daqd_stream_client
        daqd_stream_common
        nds::cxx
        Boost::boost
        Threads::Threads)
endif(NDS_CLIENT_INCLUDE_DIRS)
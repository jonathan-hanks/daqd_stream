#include "daqd_stream.hh"

#include <algorithm>
#include <iterator>
#include <cassert>
#include <cstdint>
#include <tuple>
#include <iostream>
#include <iterator>

#include <boost/iterator/zip_iterator.hpp>

namespace
{
    std::vector< daqd_stream::online_channel >
    find_chans( daqd_stream::client&              client,
                const std::vector< std::string >& selections )
    {
        std::vector< daqd_stream::online_channel > chans;
        auto local_channels = client.channels( );

        for ( const auto& name : selections )
        {
            auto it = std::find_if(
                local_channels.begin( ),
                local_channels.end( ),
                [ &name ]( const daqd_stream::online_channel& cur ) -> bool {
                    return name == cur.name.data( );
                } );
            if ( it == local_channels.end( ) )
            {
                throw std::runtime_error( "Requested a non-existant channel" );
            }
            chans.emplace_back( *it );
        }
        return chans;
    }

    template < typename T >
    void
    dump_data( std::ostream& os, const void* data, std::size_t samples )
    {
        auto cur = reinterpret_cast< const T* >( data );
        std::copy( cur, cur + samples, std::ostream_iterator< T >( os, " " ) );
    }

    void
    dump_channel( std::ostream&                       os,
                  const std::vector< char >&          buffer,
                  const daqd_stream::output_metadata& metadata )
    {
        os << metadata.name << " ";
        auto        samples = metadata.location.datarate / 16;
        const void* offset = reinterpret_cast< const void* >(
            buffer.data( ) + metadata.location.data_offset );
        switch ( metadata.location.datatype )
        {
        case daqd_stream::DATA_TYPE::INT16:
            dump_data< std::int16_t >( os, offset, samples );
            break;
        case daqd_stream::DATA_TYPE::INT32:
            dump_data< std::int32_t >( os, offset, samples );
            break;
        case daqd_stream::DATA_TYPE::INT64:
            dump_data< std::int64_t >( os, offset, samples );
            break;
        case daqd_stream::DATA_TYPE::FLOAT32:
            dump_data< float >( os, offset, samples );
            break;
        case daqd_stream::DATA_TYPE::FLOAT64:
            dump_data< double >( os, offset, samples );
            break;
        case daqd_stream::DATA_TYPE::COMPLEX32:
            os << "Unsupported";
            break;
        case daqd_stream::DATA_TYPE::UINT32:
            dump_data< std::uint32_t >( os, offset, samples );
            break;
        default:
            throw std::runtime_error( "Unknown data type found" );
        }
        os << "\n";
    }

    ComparisonType
    to_comp_type( daqd_stream::DATA_TYPE type )
    {
        switch ( type )
        {
        case daqd_stream::DATA_TYPE::INT16:
            return ComparisonType::INT16;
        case daqd_stream::DATA_TYPE::INT32:
            return ComparisonType::INT32;
        case daqd_stream::DATA_TYPE::INT64:
            return ComparisonType::INT64;
        case daqd_stream::DATA_TYPE::FLOAT32:
            return ComparisonType::FLOAT32;
        case daqd_stream::DATA_TYPE::FLOAT64:
            return ComparisonType::FLOAT64;
        case daqd_stream::DATA_TYPE::UINT32:
            return ComparisonType::UINT32;
        default:
            throw std::runtime_error( "Unknown data type found" );
        }
    }

    std::vector< ComparisonBuffer >
    to_comp_buffers(
        daqd_stream::data_status&                          status,
        const std::vector< char >&                         data,
        const std::vector< daqd_stream::output_metadata >& metadata,
        int                                                sec_offset,
        int                                                stride )
    {
        std::vector< ComparisonBuffer > out{ };
        out.reserve( metadata.size( ) );
        assert( metadata.size( ) == status.channel_status.size( ) );
        std::transform(
            boost::make_zip_iterator( boost::make_tuple(
                metadata.begin( ), status.channel_status.begin( ) ) ),
            boost::make_zip_iterator( boost::make_tuple(
                metadata.end( ), status.channel_status.end( ) ) ),
            std::back_inserter( out ),
            [ &data, sec_offset, stride ](
                const boost::tuple< const daqd_stream::output_metadata&, int >
                    info ) -> ComparisonBuffer {
                int multiplier = ( stride >= 1 ? 16 : 1 );

                auto data_type =
                    to_comp_type( boost::get< 0 >( info ).location.datatype );
                auto segment_length =
                    boost::get< 0 >( info ).location.bytes_per_16th *
                    multiplier;
                auto segment_offset = sec_offset * segment_length;
                auto buffer = std::vector< char >( segment_length, 0 );

                auto begin = data.data( ) +
                    boost::get< 0 >( info ).location.data_offset +
                    segment_offset;
                auto end = begin + segment_length;
                std::copy( begin, end, buffer.begin( ) );
                auto cur_status = boost::get< 1 >( info );
                if ( cur_status != 0 )
                {
                    // std::cerr << "status = " << cur_status << "\n";
                }
                return ComparisonBuffer{ data_type,
                                         boost::get< 1 >( info ) == 0,
                                         std::move( buffer ) };
            } );

        return out;
    }

    std::vector< ComparisonBuffer >
    to_comp_buffers(
        daqd_stream::sec_data_status&                      status,
        const std::vector< char >&                         data,
        const std::vector< daqd_stream::output_metadata >& metadata,
        int                                                sec_offset,
        int                                                stride )
    {
        std::vector< ComparisonBuffer > out{ };
        out.reserve( metadata.size( ) );
        assert( metadata.size( ) == status.channel_status.size( ) );
        std::transform(
            boost::make_zip_iterator( boost::make_tuple(
                metadata.begin( ), status.channel_status.begin( ) ) ),
            boost::make_zip_iterator( boost::make_tuple(
                metadata.end( ), status.channel_status.end( ) ) ),
            std::back_inserter( out ),
            [ &data, sec_offset, stride ](
                const boost::tuple< const daqd_stream::output_metadata&,
                                    std::array< unsigned int, 16 > > info )
                -> ComparisonBuffer {
                int multiplier = ( stride >= 1 ? 16 : 1 );

                auto data_type =
                    to_comp_type( boost::get< 0 >( info ).location.datatype );
                auto segment_length =
                    boost::get< 0 >( info ).location.bytes_per_16th *
                    multiplier;
                auto segment_offset = sec_offset * segment_length;
                auto buffer = std::vector< char >( segment_length, 0 );

                auto begin = data.data( ) +
                    boost::get< 0 >( info ).location.data_offset +
                    segment_offset;
                auto end = begin + segment_length;
                std::copy( begin, end, buffer.begin( ) );
                auto         status_array = boost::get< 1 >( info );
                unsigned int cur_status{ 0 };
                for ( const auto& entry : status_array )
                {
                    cur_status |= entry;
                }
                if ( cur_status != 0 )
                {
                    // std::cerr << "status = " << cur_status << "\n";
                }
                return ComparisonBuffer{ data_type,
                                         cur_status == 0,
                                         std::move( buffer ) };
            } );

        return out;
    }

} // namespace

bool debug_daqd = false;

void
daqd_stream_main_loop( Comparator&                       comp,
                       int                               slot,
                       std::atomic< bool >&              finished,
                       daqd_stream::client&              client,
                       const std::vector< std::string >& selections,
                       int                               stride )
{
    auto plan_type = ( stride < 0 ? daqd_stream::PLAN_TYPE::PLAN_16TH
                                  : daqd_stream::PLAN_TYPE::PLAN_SEC );

    auto selected_chans = find_chans( client, selections );
    auto plan = client.plan_request(
        plan_type, selections, ( stride < 0 ? 0 : stride ) );

    auto buffer_layout = plan.metadata( );

    auto                buf_size = plan.required_size( );
    std::vector< char > buffer{ };
    buffer.resize( buf_size );
    auto         last_seen = daqd_stream::make_time_point( 0, 0 );
    std::int64_t start_second{ 0 };
    while ( !finished )
    {
        daqd_stream::data_status     status;
        daqd_stream::sec_data_status sec_status;
        if ( stride < 0 )
        {
            client.get_16th_data(
                plan, last_seen, buffer.data( ), buffer.size( ), status );
            if ( debug_daqd )
            {
                std::cout << "d " << status.gps.seconds << ":"
                          << status.gps.nano << "\n";
            }
        }
        else
        {
            client.get_sec_data( plan,
                                 start_second,
                                 stride,
                                 buffer.data( ),
                                 buffer.size( ),
                                 sec_status );
            if ( debug_daqd )
            {
                std::cout << "d " << sec_status.gps.seconds << ":"
                          << sec_status.gps.nano << "\n";
            }
        }

        if ( stride <= 0 )
        {
            comp.input_cycle(
                slot,
                status.gps.seconds,
                status.gps.nano,
                to_comp_buffers( status, buffer, buffer_layout, 0, stride ) );
            start_second = status.gps.seconds + 1;
        }
        else
        {
            for ( auto sec = 0; sec < stride; ++sec )
            {
                comp.input_cycle(
                    slot,
                    sec_status.gps.seconds + sec,
                    0,
                    to_comp_buffers(
                        sec_status, buffer, buffer_layout, sec, stride ) );
            }
            start_second = sec_status.gps.seconds + stride;
        }
        last_seen = status.gps;
    }
}

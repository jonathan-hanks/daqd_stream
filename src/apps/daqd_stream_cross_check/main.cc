#include <algorithm>
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <iterator>
#include <random>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#include <boost/algorithm/string.hpp>

#include <daqd_stream/daqd_stream.hh>

#include "compare.hh"
#include "daqd_stream.hh"
#include "nds_stream.hh"

std::atomic< bool >* finished_signal{ nullptr };
void
signal_handler( int dummy )
{
    if ( finished_signal )
    {
        std::cerr << "signal caught\n";
        *finished_signal = true;
    }
    else
    {
        std::cerr << "no handler !!!\n";
    }
}

void
terminate_handler( )
{
    std::cerr << "terminate handler called\n";
}

class ConfigError : public std::runtime_error
{
public:
    explicit ConfigError( const char* msg ) : std::runtime_error( msg )
    {
    }
    explicit ConfigError( const std::string& msg ) : std::runtime_error( msg )
    {
    }
    ~ConfigError( ) override = default;
};

struct NdsConfig
{
    std::string hostname{ "localhost" };
    int         port{ 31200 };
};

std::ostream&
operator<<( std::ostream& os, const NdsConfig& cfg )
{
    os << cfg.hostname << ":" << cfg.port;
    return os;
}

struct Config
{
    std::string                daq_segment{ "daqd_stream" };
    std::vector< NdsConfig >   nds_servers;
    std::vector< std::string > channels;
    int                        stride{ -1 };
};

std::ostream&
operator<<( std::ostream& os, const Config& cfg )
{
    os << "daq:   " << cfg.daq_segment << "\n"
       << "nds:   ";
    std::copy( cfg.nds_servers.begin( ),
               cfg.nds_servers.end( ),
               std::ostream_iterator< NdsConfig >( os, ", " ) );
    os << "\n"
       << "chans: ";
    std::copy( cfg.channels.begin( ),
               cfg.channels.end( ),
               std::ostream_iterator< std::string >( os, ", " ) );
    os << "\n";
    return os;
}

void
usage( )
{
    std::cout
        << "Usage:\n"
        << "--daq <segment>      - defaults [daqd_stream]\n"
        << "--nds <server:port>  - space separated list of nds server "
           "host:port\n"
        << "--channels <channel> - space separated list of channel names\n"
        << "--stride <seconds>   - data stride, -1 is 16Hz [-1]\n"
        << "--debug-daq          - debug for daq stream sources\n"
        << "--debug-nds          - debug for nds sources\n"
        << "-h|--help            - this help\n"
        << "\nIf channels are not specified then they are randomly selected "
           "from the daq_segment\n";

    std::exit( 1 );
}

NdsConfig
parse_nds_config( const std::string& input )
{
    NdsConfig cfg;
    auto      index = input.find( ':' );
    if ( index == std::string::npos )
    {
        cfg.hostname = input;
    }
    else
    {
        cfg.hostname = input.substr( 0, index );
        auto tmp = input.substr( index + 1 );
        cfg.port = std::atoi( tmp.c_str( ) );
        if ( cfg.port == 0 )
        {
            throw ConfigError( "Nds servers are specified as host:port" );
        }
    }
    return cfg;
}

std::vector< NdsConfig >
parse_nds_servers( const std::string& input )
{
    std::vector< NdsConfig > servers{ };

    std::vector< std::string > server_names{ };
    boost::split( server_names, input, boost::is_space( ) );
    std::transform( server_names.begin( ),
                    server_names.end( ),
                    std::back_inserter( servers ),
                    parse_nds_config );

    return servers;
}

std::vector< std::string >
parse_channels( const std::string& input )
{
    std::vector< std::string > channels;
    boost::split( channels, input, boost::is_space( ) );
    return channels;
}

template < typename T >
T
parse( const std::string& input )
{
    std::istringstream is{ input };
    T                  results;
    is >> results;
    return results;
}

Config
parse_args( int argc, char** argv )
{
    std::deque< std::string > args;
    for ( int i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }

    auto req_argument = [ &args ]( const char* field ) -> std::string {
        if ( args.empty( ) )
        {
            throw ConfigError( field );
        }
        std::string tmp = args.front( );
        args.pop_front( );
        return tmp;
    };

    Config cfg;
    try
    {
        while ( !args.empty( ) )
        {
            std::string arg = args.front( );
            args.pop_front( );

            if ( arg == "-h" || arg == "--help" )
            {
                throw ConfigError( "" );
            }
            else if ( arg == "--daq" )
            {
                cfg.daq_segment = req_argument( "--daqd requires an argument" );
            }
            else if ( arg == "--nds" )
            {
                cfg.nds_servers = parse_nds_servers(
                    req_argument( "--nds requires an argument" ) );
            }
            else if ( arg == "--channels" )
            {
                cfg.channels = parse_channels(
                    req_argument( "--channels requires an argument" ) );
            }
            else if ( arg == "--debug-daq" )
            {
                debug_daqd = true;
            }
            else if ( arg == "--debug-nds" )
            {
                debug_nds = true;
            }
            else if ( arg == "--stride" )
            {
                cfg.stride = parse< int >(
                    req_argument( "--stride requires an argument" ) );
                if ( cfg.stride < -1 || cfg.stride > 5 || cfg.stride == 0 )
                {
                    throw ConfigError( "--stride must be -1 or [1..5]" );
                }
            }
        }
        if ( cfg.nds_servers.empty( ) )
        {
            throw ConfigError( "At least one nds server must be configured" );
        }
    }
    catch ( ConfigError& err )
    {
        std::cerr << err.what( ) << "\n";
        usage( );
    }
    return cfg;
}

std::vector< std::string >
select_random_channels(
    std::random_device&                               rd,
    const std::vector< daqd_stream::online_channel >& available )
{
    std::vector< std::string > channels{ };

    if ( available.size( ) < 10 )
    {
        throw std::runtime_error( "Too few channels to choose from" );
    }
    std::default_random_engine                   r( rd( ) );
    std::uniform_int_distribution< std::size_t > dist( 0,
                                                       available.size( ) - 1 );

    int count = 0;
    while ( count < 10 )
    {
        std::string selection( available[ dist( r ) ].name.data( ) );

        if ( std::find( channels.begin( ), channels.end( ), selection ) ==
             channels.end( ) )
        {
            channels.emplace_back( std::move( selection ) );
            ++count;
        }
    }
    return channels;
}

int
main( int argc, char* argv[] )
{
    std::random_device  rd;
    std::atomic< bool > finished{ false };
    finished_signal = &finished;

    auto cfg = parse_args( argc, argv );
    std::cout << "Configured as:\n" << cfg;

    auto client = daqd_stream::client::create( "daqd_stream" );

    auto local_channels = client->channels( );
    if ( cfg.channels.empty( ) )
    {
        cfg.channels = select_random_channels( rd, local_channels );
    }
    std::cout << "Channels:\n";
    std::copy( cfg.channels.begin( ),
               cfg.channels.end( ),
               std::ostream_iterator< std::string >( std::cout, "\n" ) );
    std::cout << std::endl;

    for ( const auto& entry : { SIGINT, SIGTERM, SIGABRT } )
    {
        if ( std::signal( entry, signal_handler ) == SIG_ERR )
        {
            throw std::runtime_error( "Could not add signal handler" );
        }
    }
    std::set_terminate( terminate_handler );

    auto       comparisons = 1 + cfg.nds_servers.size( );
    Comparator comp( comparisons,
                     cfg.channels,
                     ( cfg.stride < 0
                           ? Comparator::stride_type::FAST_STRIDE
                           : Comparator::stride_type::SLOW_STRIDE ) );

    std::vector< std::thread > threads;
    threads.reserve( comparisons );

    threads.emplace_back( [ &comp, &finished, &client, &cfg ]( ) {
        daqd_stream_main_loop(
            comp, 0, finished, *client, cfg.channels, cfg.stride );
    } );

    for ( auto i = 0; i < cfg.nds_servers.size( ); ++i )
    {
        threads.emplace_back( [ &comp, slot = i + 1, i, &finished, &cfg ]( ) {
            nds_stream_main_loop( comp,
                                  slot,
                                  finished,
                                  cfg.nds_servers[ i ].hostname,
                                  cfg.nds_servers[ i ].port,
                                  cfg.channels,
                                  cfg.stride );
        } );
    }
    while ( !finished )
    {
        std::this_thread::sleep_for( std::chrono::microseconds( 100 ) );
    }
    std::cout << "finished"
              << "\n";
    return 0;
}

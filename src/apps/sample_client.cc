//
// Created by jonathan.hanks on 2/18/22.
//
#include <atomic>
#include <csignal>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <daqd_stream/daqd_stream.hh>

std::atomic< bool > done{ false };

bool print_all{ false };

void
signal_handler( int dummy )
{
    done = true;
}

template < typename T >
void
print_samples( T* data, int count )
{
    count = ( print_all ? count : 16 );
    for ( auto i = 0; i < count; ++i )
    {
        std::cout << ( i % 16 == 0 && i > 0 ? "\n" : " " ) << data[ i ];
    }
}

void
print_sample( char* data, daqd_stream::DATA_TYPE type, int count )
{
    using DT = daqd_stream::DATA_TYPE;

    switch ( type )
    {
    case DT::FLOAT32:
        print_samples( (float*)data, count );
        break;
    case DT::FLOAT64:
        print_samples( (double*)data, count );
        break;
    case DT::INT16:
        print_samples( (std::int16_t*)data, count );
        break;
    case DT::INT32:
        print_samples( (std::int32_t*)data, count );
        break;
    case DT::INT64:
        print_samples( (std::int64_t*)data, count );
        break;
    case DT::UINT32:
        print_samples( (std::uint32_t*)data, count );
        break;
    default:
        std::cout << "unknown type";
    }
}

void
test_16th( daqd_stream::client& client, const std::vector< std::string > names )
{
    auto plan =
        client.plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH, names, 0 );
    std::vector< char > buffer{ };
    buffer.resize( plan.required_size( ) );
    daqd_stream::time_point  last_seen{ };
    daqd_stream::data_status status{ };
    auto                     meta = plan.metadata( );

    while ( !done )
    {
        client.get_16th_data(
            plan, last_seen, buffer.data( ), buffer.size( ), status );
        std::cout << status.gps.seconds << ":" << status.gps.nano << "\n";
        last_seen = status.gps;

        std::cout << "\t";
        for ( const auto& cur_meta : meta )
        {
            auto offset = cur_meta.location.data_offset;
            auto data_type = cur_meta.location.datatype;
            print_sample( buffer.data( ) + offset,
                          data_type,
                          cur_meta.location.datarate / 16 );
            std::cout << " " << cur_meta.name << " ";
        }
        std::cout << "\n";
    }
}

void
test_sec( daqd_stream::client& client, const std::vector< std::string > names )
{
    auto plan =
        client.plan_request( daqd_stream::PLAN_TYPE::PLAN_SEC, names, 1 );
    std::vector< char > buffer{ };
    buffer.resize( plan.required_size( ) );
    std::int64_t                 req_gps = 0;
    daqd_stream::sec_data_status status{ };
    auto                         meta = plan.metadata( );

    while ( !done )
    {
        client.get_sec_data( plan,
                             req_gps,
                             1,
                             buffer.data( ),
                             buffer.size( ),
                             status,
                             daqd_stream::byte_order::NATIVE );
        std::cout << status.gps.seconds << ":" << status.gps.nano << "\n";
        req_gps = status.gps.seconds + 1;

        for ( const auto& cur_meta : meta )
        {
            auto offset = cur_meta.location.data_offset;
            auto data_type = cur_meta.location.datatype;
            std::cout << "\t";
            print_sample( buffer.data( ) + offset,
                          data_type,
                          cur_meta.location.datarate );
            std::cout << " " << cur_meta.name << "\n";
        }
        std::cout << "\n";
    }
}

int
main( int argc, char* argv[] )
{
    const static std::string PRINT_ALL{ "print_all" };

    if ( argc < 2 )
    {
        std::cout << "usage\n\t" << argv[ 0 ]
                  << " <daqd_stream_name> [optional channels]\n";
        std::exit( 1 );
    }
    auto client = daqd_stream::client::create( argv[ 1 ] );

    auto channels = client->channels( );

    std::cout << "Got " << channels.size( ) << " channels\n";

    std::vector< std::string > names{ };
    for ( auto i = 2; i < argc; ++i )
    {
        if ( PRINT_ALL == argv[ i ] )
        {
            print_all = true;
        }
        else
        {
            names.emplace_back( argv[ i ] );
        }
    }
    if ( names.empty( ) )
    {
        names.emplace_back( channels[ 0 ].name.data( ) );
        if ( channels.size( ) > 1024 )
        {
            names.emplace_back( channels[ 1024 ].name.data( ) );
        }
        for ( const auto& name : names )
        {
            std::cout << name << "\n";
        }
    }
    std::signal( SIGINT, signal_handler );
    // test_16th(*client, names);
    test_sec( *client, names );

    return 0;
}
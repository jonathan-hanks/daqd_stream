/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include <iostream>
#include <daqd_stream/daqd_stream.hh>

int
main( )
{
    std::cout << "Hello, World!" << std::endl;

    auto daq_client = daqd_stream::client::create( "daq_lite" );

    auto chans = daq_client->channels( );
    for ( const auto& chan : chans )
    {
        std::cout << chan.name.data( ) << "\n";
    }

    daqd_stream::channel_name_list chan_names = {
        "mod9-909--gpssmd100koffc1p--15--2--16",
        "mod9-895--gpssmd100koffc1p--1--2--16",
        "mod9-896--gpssmd100koffc1p--2--2--16",
        "mod9-774--gpssmd100koffc1p--6--2--16"
    };
    std::vector< std::size_t > sizes = daq_client->size_list( chan_names );
    for ( int i = 0; i < sizes.size( ); ++i )
    {
        std::cout << chan_names[ i ] << " " << sizes[ i ] << "\n";
    }

    auto plan16th = daq_client->plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH,
                                              chan_names );
    auto plansec = daq_client->plan_request( daqd_stream::PLAN_TYPE::PLAN_16TH,
                                             chan_names );

    std::vector< char > buf;
    buf.resize( plansec.required_size( ) );

    daqd_stream::time_point  last_seen;
    daqd_stream::data_status status;
    for ( int i = 0; i < 5; ++i )
    {
        daq_client->get_16th_data(
            plan16th, last_seen, buf.data( ), buf.size( ), status );
        last_seen = status.gps;
    }
    //    for ( int i = 0; i < 5; ++i )
    //    {
    //        daq_client.get_sec_data(
    //            plansec, < #initializer # >, buf.data( ), buf.size( ), status
    //            );
    //    }
    std::cout << "Goodbye World!" << std::endl;

    return 0;
}

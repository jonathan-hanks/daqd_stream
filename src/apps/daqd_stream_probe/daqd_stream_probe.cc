//
// Created by jonathan.hanks on 9/16/21.
//
#include <iostream>
#include <deque>
#include <sstream>
#include <stdexcept>
#include <string>
#include "compare_data.hh"
#include "internal.hh"
#include "memory_buffer.hh"
#include "simple_range.hh"
#include "multi_source_info_block.hh"

#include "../../vendor/advligorts/daq_metadata.h"

enum class probe_actions
{
    DUMP,
    LIST_CHANNELS,
    DUMP_CONFIG_HASH,
    DUMP_BLOCK_HEADERS,
    DUMP_MULTI_SOURCE_BLOCK,
    COMPARE_DATA,
};

struct Config_Options
{
    std::vector< std::string > segment_names{ "daqd_stream" };
    probe_actions              action{ probe_actions::DUMP };
};

class cmdline_error : std::runtime_error
{
public:
    explicit cmdline_error( const std::string& msg ) : std::runtime_error( msg )
    {
    }
    virtual ~cmdline_error( ) = default;
};

std::string
requires_argument_msg( const char* arg )
{
    std::ostringstream os;
    os << arg << " requires an argument";
    return os.str( );
}

std::string
unknown_argument_msg( const std::string& arg )
{
    std::ostringstream os;
    os << "'" << arg << "' is an unknown argument";
    return os.str( );
}

void
print_help( const char* program_name )
{
    Config_Options defaults{ };
    std::cout << "Usage:\n\t" << program_name << " <options> <action>\n";
    std::cout << "Options:\n";
    std::cout << "-h|--help\t\t\tThis help.\n";
    std::cout << "-s|--segment <name>\tShared memory segment name ["
              << defaults.segment_names[ 0 ] << "]\n";
    std::cout << "Actions:\n";
    std::cout
        << "dump\t\t\t\tDump the contents of the buffer [default action]\n";
    std::cout << "dump_block_headers\tDump the headers of the data blocks\n";
    std::cout << "channels\t\t\tList the channels of the buffer\n";
    std::cout << "config\t\t\t\tPrint the configuration hash information\n";
    std::cout << "dump_multi\t\t\tDump the contents of the buffer which must\n";
    std::cout << "\t\t\t\t\tbe a multi source buffer\n";
    std::cout << "compare_data\t\tCompare the common data segments between two "
                 "buffers\n";
    std::cout << "\nSegments can be names in /dev/shm (no need for the "
                 "directory) or images of them mmapped mmap://filepath\n";
}

Config_Options
parse_arguments( int argc, char* argv[] )
{
    using action_map_t = std::map< std::string, probe_actions >;

    Config_Options                         opts{ };
    std::deque< std::string >              args{ };
    std::map< std::string, probe_actions > action_map{
        action_map_t::value_type( "dump", probe_actions::DUMP ),
        action_map_t::value_type( "channels", probe_actions::LIST_CHANNELS ),
        action_map_t::value_type( "config", probe_actions::DUMP_CONFIG_HASH ),
        action_map_t::value_type( "dump_block_headers",
                                  probe_actions::DUMP_BLOCK_HEADERS ),
        action_map_t::value_type( "dump_multi",
                                  probe_actions::DUMP_MULTI_SOURCE_BLOCK ),
        action_map_t::value_type( "compare_data", probe_actions::COMPARE_DATA ),
    };

    std::vector< std::string > segment_names{ };

    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }
    auto is_reserved = [ &action_map ]( const std::string& arg ) -> bool {
        return action_map.find( arg ) != action_map.end( );
    };
    auto is_arg = []( const std::string& arg ) -> bool {
        return ( arg.find( '-' ) == 0 );
    };
    auto pop = [ &args ]( ) -> std::string {
        std::string val = args.front( );
        args.pop_front( );
        return val;
    };
    auto pop_multi = [ &args,
                       &pop,
                       &is_reserved,
                       &is_arg ]( ) -> std::vector< std::string > {
        std::vector< std::string > results;
        while ( !args.empty( ) )
        {
            std::string cur = pop( );
            if ( is_reserved( cur ) || is_arg( cur ) )
            {
                args.push_front( std::move( cur ) );
                break;
            }
            results.emplace_back( std::move( cur ) );
        }
        return results;
    };

    try
    {
        while ( !args.empty( ) )
        {
            auto arg{ pop( ) };
            if ( arg == "--segment" || arg == "-s" )
            {
                segment_names = pop_multi( );
                if ( segment_names.empty( ) )
                {
                    throw cmdline_error(
                        requires_argument_msg( "-s|--segment" ) );
                }
            }
            else if ( action_map.find( arg ) != action_map.end( ) )
            {
                opts.action = action_map[ arg ];
            }
            else if ( arg == "--help" || arg == "-h" )
            {
                print_help( argv[ 0 ] );
                exit( 0 );
            }
            else
            {
                throw cmdline_error( unknown_argument_msg( arg ) );
            }
        }
    }
    catch ( cmdline_error& err )
    {
        print_help( argv[ 0 ] );
        exit( 1 );
    }
    if ( !segment_names.empty( ) )
    {
        opts.segment_names = std::move( segment_names );
    }
    return opts;
}

void
handle_dump( Memory_buffer& shmem )
{
    auto* header = shmem.get_as< daqd_stream::detail::shared_mem_header >( );

    auto offset_of = [ header ]( char* ptr ) -> std::ptrdiff_t {
        auto start = reinterpret_cast< char* >( &header->magic_bytes );
        return ptr - start;
    };

    std::cout << "Magic:        " << std::hex << header->magic_bytes << std::dec
              << "\n";
    std::cout << "Struct Id:    " << header->struct_id << "\n";
    std::cout << "Version:      " << header->version << "\n";
    std::cout << "cur_block:    " << header->get_cur_block( ) << "\n";
    std::cout << "num_blocks:   " << header->num_blocks << "\n";
    std::cout << "segment_size: " << header->segment_size << "\n";
    std::cout << "\n";
    auto cur_dyn = header->bump_.get( );
    std::cout << "dynamic memory start:      "
              << offset_of( header->data_begin( ) ) << "\n";
    std::cout << "free dynamic memory start: " << offset_of( cur_dyn ) << "\n";
    std::cout << "dynamic memory end:        "
              << offset_of( header->data_end( ) ) << "\n";
    std::cout << "dynamic mem free/used:     " << header->data_end( ) - cur_dyn
              << "/" << cur_dyn - header->data_begin( ) << "\n";
    auto block = header->blocks( header->get_cur_block( ) );
    std::cout << "gps: " << block.gps_second << ":" << block.cycle << "\n";

    auto* stream_config = header->get_config( );
    auto& channel_list = stream_config->channels;
    std::cout << "\n";
    std::cout << "Current channel list size: " << channel_list.size( ) << "\n";
}

void
handle_list_channels( Memory_buffer& shmem )
{
    auto* header = shmem.get_as< daqd_stream::detail::shared_mem_header >( );

    auto offset_of = [ header ]( char* ptr ) -> std::ptrdiff_t {
        auto start = reinterpret_cast< char* >( &header->magic_bytes );
        return ptr - start;
    };

    auto* stream_config = header->get_config( );
    auto& channel_list = stream_config->channels;
    std::cout << "\n";
    std::cout << "Current channel list size: " << channel_list.size( ) << "\n";
    for ( const auto& chan : channel_list )
    {
        std::cout << chan.name.data( ) << "\n";
    }
}

void
handle_dump_config_hash( Memory_buffer& shmem )
{
    auto* header = shmem.get_as< daqd_stream::detail::shared_mem_header >( );

    const auto& checksums = header->config_->checksums;
    std::cout << "master config = " << std::hex << checksums.master_config
              << std::dec << "\n";
    for ( auto i = 0; i < checksums.dcu_config.size( ); ++i )
    {
        std::cout << "\t" << i << " " << std::hex << checksums.dcu_config[ i ]
                  << std::dec << "\n";
    }
}

void
handle_dump_block_headers( Memory_buffer& shmem )
{
    auto* header = shmem.get_as< daqd_stream::detail::shared_mem_header >( );

    auto offset_of = [ header ]( char* ptr ) -> std::ptrdiff_t {
        auto start = reinterpret_cast< char* >( &header->magic_bytes );
        return ptr - start;
    };

    Simple_range< daqd_stream::detail::data_block* > blocks(
        header->blocks_begin( ), header->blocks_end( ) );
    for ( const auto& block : blocks )
    {
        auto good_configs = std::count_if(
            block.dcu_status.begin( ),
            block.dcu_status.end( ),
            []( std::uint32_t status ) -> bool { return status == 2; } );
        std::cout << block.gps_second << " " << block.cycle << " ("
                  << good_configs << ")\n";
    }
}

void
handle_dump_multi_block( Memory_buffer& shmem )
{
    auto multi_header =
        shmem.get_as< daqd_stream::detail::multi_source_info_block >( );
    if ( multi_header->magic != daqd_stream::detail::multi_source_magic )
    {
        throw std::runtime_error( "Invalid multi source header block" );
    }
    std::cout << "multi source\n";
    std::cout << "version: " << multi_header->version << "\n";
    auto range = multi_header->block_range;
    auto start = daqd_stream::detail::decode_block_range_start( range );
    auto stop = daqd_stream::detail::decode_block_range_end( range );
    std::cout << "start: " << start << "\n";
    std::cout << "end:   " << stop << "\n";
    std::cout << "sources:\n";
    for ( auto i = start; i < stop; ++i )
    {
        std::cout << "\t" << multi_header->sources[ i ].data( ) << "\n";
    }
}

int
main( int argc, char* argv[] )
{
    using simple_action_map =
        std::map< probe_actions, void ( * )( Memory_buffer& ) >;
    auto              opts = parse_arguments( argc, argv );
    simple_action_map simple_actions{
        simple_action_map::value_type( probe_actions::DUMP, handle_dump ),
        simple_action_map::value_type( probe_actions::LIST_CHANNELS,
                                       handle_list_channels ),
        simple_action_map::value_type( probe_actions::DUMP_CONFIG_HASH,
                                       handle_dump_config_hash ),
        simple_action_map::value_type( probe_actions::DUMP_BLOCK_HEADERS,
                                       handle_dump_block_headers ),
        simple_action_map::value_type( probe_actions::DUMP_MULTI_SOURCE_BLOCK,
                                       handle_dump_multi_block ),
    };

    auto it = simple_actions.find( opts.action );
    if ( it != simple_actions.end( ) )
    {
        for ( const auto& segment_name : opts.segment_names )
        {
            std::cout << "\nShared memory segment '" << segment_name << "'\n\n";
            Memory_buffer buffer( segment_name );
            it->second( buffer );
        }
    }
    else if ( opts.action == probe_actions::COMPARE_DATA )
    {
        if ( opts.segment_names.size( ) != 2 )
        {
            throw std::runtime_error( "Two segments must be specified to do "
                                      "the compare_data action" );
        }
        Memory_buffer first_buffer( opts.segment_names[ 0 ] );
        Memory_buffer second_buffer( opts.segment_names[ 1 ] );
        handle_compare_data( first_buffer, second_buffer );
    }
    else
    {
        throw std::runtime_error( "Unhandled command" );
    }

    return 0;
}
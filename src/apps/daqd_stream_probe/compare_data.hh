//
// Created by jonathan.hanks on 9/22/22.
//

#ifndef DAQD_STREAM_PROBE_COMPARE_DATA_HH
#define DAQD_STREAM_PROBE_COMPARE_DATA_HH

#include <memory_buffer.hh>

void handle_compare_data( Memory_buffer& a, Memory_buffer& b );

#endif // DAQD_STREAM_PROBE_COMPARE_DATA_HH

/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#include "checksum.hh"
#include <cstring>
#include <type_traits>

extern "C" {
#include "crc.h"
}

namespace
{

    template < typename T >
    void
    checksum_general( daqd_stream::detail::checksum_generator& gen,
                      const T&                                 obj )
    {
        gen.hash( reinterpret_cast< const char* >( &obj ), sizeof( obj ) );
    }

    void
    checksum_string( daqd_stream::detail::checksum_generator& gen,
                     const char*                              str )
    {
        gen.hash( str, std::strlen( str ) );
    }

    template < size_t N >
    void
    checksum_fixed_str( daqd_stream::detail::checksum_generator& gen,
                        const daqd_stream::fixed_string< N >&    str )
    {
        gen.hash( str.data( ), std::strlen( str.data( ) ) );
    }
} // namespace

namespace daqd_stream
{
    namespace detail
    {

        static_assert( std::is_same< unsigned int, checksum_t >::value,
                       "checksum_t must match unsigned int" );

        void
        checksum_generator::hash( const online_channel& channel )
        {
            checksum_fixed_str( *this, channel.name );
            checksum_general( *this, channel.dcuid );
            checksum_general( *this, channel.datatype );
            checksum_general( *this, channel.datarate );
            checksum_general( *this, channel.bytes_per_16th );
            checksum_general( *this, channel.data_offset );
            checksum_general( *this, channel.signal_gain );
            checksum_general( *this, channel.signal_slope );
            checksum_general( *this, channel.signal_offset );
            checksum_fixed_str( *this, channel.units );
        }

        void
        checksum_generator::hash( const char* data, std::size_t size )
        {
            sum_ = crc_ptr( const_cast< char* >( data ), size, sum_ );
            len_consumed_ += size;
        }

        checksum_t
        checksum_generator::get_checksum( ) const
        {
            return crc_len( len_consumed_, sum_ );
        }
    } // namespace detail
} // namespace daqd_stream
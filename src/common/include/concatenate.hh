/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_CONCATENATE_HH
#define DAQD_STREAM_CONCATENATE_HH

#include <string>

namespace daqd_stream
{

    template < typename StringLike >
    std::string
    concatenate( const StringLike s )
    {
        return s;
    }

    template < typename StringLike, typename... Args >
    std::string
    concatenate( const StringLike s, Args... args )
    {
        return s + concatenate( args... );
    }
} // namespace daqd_stream

#endif // DAQD_STREAM_CONCATENATE_HH

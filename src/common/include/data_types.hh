//
// Created by jonathan.hanks on 10/27/21.
//

#ifndef DAQD_STREAM_DATA_TYPES_HH
#define DAQD_STREAM_DATA_TYPES_HH

#include <daqd_stream/basic_types.hh>

namespace daqd_stream
{
    namespace detail
    {
        /**
         * @brief Return the size of a given DATA_TYPE element
         * @param dt the data type to get size information regarding
         * @return size, 0 on error
         */
        inline int
        data_type_size( DATA_TYPE dt ) noexcept
        {
            switch ( dt )
            {
            case DATA_TYPE::INT16:
                return 2;
            case DATA_TYPE::INT32:
            case DATA_TYPE::FLOAT32:
            case DATA_TYPE::UINT32:
                return 4;
            case DATA_TYPE::INT64:
            case DATA_TYPE::FLOAT64:
            case DATA_TYPE::COMPLEX32:
                return 8;
            case DATA_TYPE::UNDEFINED:
            default:
                return 0;
            }
        }
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_DATA_TYPES_HH

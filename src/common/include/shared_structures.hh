/*
Copyright 2022 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_SHARED_STRUCTURES_HH
#define DAQD_STREAM_SHARED_STRUCTURES_HH

#include <algorithm>
#include <map>

#include "checksum.hh"

#include <daqd_stream/dcu_identifier.hh>

namespace daqd_stream
{
    namespace detail
    {

        struct ifo_checksums
        {
            checksum_t                           master_config{ 0 };
            std::array< checksum_t, max_dcu( ) > dcu_config{ };

            ifo_checksums( ) = default;

            ifo_checksums( const ifo_checksums& other )
                : master_config( other.master_config ),
                  dcu_config( other.dcu_config )
            {
            }

            ifo_checksums& operator=( const ifo_checksums& ) noexcept = default;

            template <
                typename GeneratorContainer,
                typename t = decltype( GeneratorContainer::dcu_checksums ) >
            ifo_checksums&
            operator=( GeneratorContainer& generators )
            {
                std::transform(
                    generators.dcu_checksums.begin( ),
                    generators.dcu_checksums.end( ),
                    dcu_config.begin( ),
                    []( const checksum_generator& gen ) -> checksum_t {
                        return gen.get_checksum( );
                    } );
                recalculate_master_config( );
                return *this;
            }

            void
            recalculate_master_config( )
            {
                master_config = calculate_master_config( dcu_config );
            }

            checksum_t
            lookup_checksum( const dcu_identifier& index ) const
            {
                if ( index.source > 0 )
                {
                    throw std::range_error( "invalid source number" );
                }
                return dcu_config[ index.dcuid ];
            }
        };

        struct unpack_second
        {
            template < typename PairType >
            auto
            operator( )( const PairType& p ) const noexcept -> auto
            {
                return p.second;
            }
        };

        struct sparse_ifo_checksums
        {
            using container_type = std::map< dcu_identifier, checksum_t >;
            void
            recalculate_master_config( )
            {
                master_config =
                    calculate_master_config< container_type, unpack_second >(
                        dcu_config );
            }
            checksum_t
            lookup_checksum( const dcu_identifier& index ) const
            {
                auto it = dcu_config.find( index );
                return ( it != dcu_config.end( ) ? it->second : 0 );
            }

            checksum_t     master_config{ 0 };
            container_type dcu_config{ };
        };

        /*!
         * @brief Final representation of the configuration
         */
        template < typename ChanList, typename ChecksumType = ifo_checksums >
        struct ifo_config_base
        {
            static_assert( std::is_same< typename ChanList::value_type,
                                         online_channel >::value,
                           "ChanList must be a container of online_channels" );

            ifo_config_base( ) = default;
            ifo_config_base( const ifo_config_base& other ) = default;

            ifo_config_base( ifo_config_base&& other ) noexcept
                : checksums{ other.checksums },
                  channels{ std::move( other.channels ) }
            {
            }

            template < typename OtherChanList >
            explicit ifo_config_base(
                const ifo_config_base< OtherChanList >& other )
                : checksums{ other.checksums }, channels{ other.size( ) }
            {
                static_assert(
                    std::is_same< typename OtherChanList::value_type,
                                  online_channel >::value,
                    "OtherChanList must be a container of online channels" );
                std::copy( other.begin( ), other.end( ), channels.begin( ) );
            }

            template < typename OtherChanList, typename AllocatorF >
            ifo_config_base( const ifo_config_base< OtherChanList >& other,
                             AllocatorF&&                            allocator )
                : checksums{ other.checksums },
                  channels( allocator( other.channels.size( ) ),
                            other.channels.size( ) )
            {
                checksums = other.checksums;
                std::copy( other.channels.begin( ),
                           other.channels.end( ),
                           channels.begin( ) );
            }

            ifo_config_base&
            operator=( const ifo_config_base& other ) = default;

            template <
                typename GeneratorContainer,
                typename t = decltype( GeneratorContainer::dcu_checksums ) >
            ifo_config_base&
            operator=( GeneratorContainer&& generators )
            {
                checksums = generators;
                channels = std::move( generators.channels );
                return *this;
            }

            //            ifo_config_base&
            //            operator=( ini_parser_intl&& ini )
            //            {
            //                checksums = ini;
            //                channels = std::move( ini.channels );
            //                return *this;
            //            }

            //            typename ChanList::const_iterator
            //            find_channel( const char* name )
            //            {
            //                auto it = std::lower_bound( channels.cbegin( ),
            //                                            channels.cend( ),
            //                                            name,
            //                                            detail::search_by_name(
            //                                            ) );
            //                return it;
            //            }

            ChecksumType checksums{ };
            ChanList     channels{ };
        };
        using ifo_config = ifo_config_base< std::vector< online_channel > >;
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_SHARED_STRUCTURES_HH

#ifndef DAQD_STREAM_PRIVATE_WAIT_GROUP_HH
#define DAQD_STREAM_PRIVATE_WAIT_GROUP_HH

#include <thread>

namespace daqd_stream
{
    namespace detail
    {

        class wait_group
        {
            using mutex_type = std::mutex;
            using lock_type = std::unique_lock< mutex_type >;

        public:
            wait_group( ) = default;
            wait_group( const wait_group& ) = delete;
            wait_group( wait_group&& ) = delete;
            wait_group& operator=( const wait_group& ) = delete;
            wait_group& operator=( wait_group&& ) = delete;

            void
            add( int count )
            {
                if ( count < 0 )
                {
                    throw std::runtime_error( "negative count passed to add" );
                }
                lock_type l_{ m_ };
                wait_count_ += count;
            }

            void
            done( )
            {
                int count = 0;
                {
                    lock_type l_{ m_ };
                    if ( wait_count_ > 0 )
                    {
                        wait_count_--;
                    }
                    count = wait_count_;
                }
                if ( count == 0 )
                {
                    signal_.notify_all( );
                }
            }

            void
            wait( )
            {
                lock_type l_{ m_ };
                while ( wait_count_ != 0 )
                {
                    signal_.wait( l_ );
                }
            }

        private:
            mutex_type              m_{ };
            std::condition_variable signal_{ };
            int                     wait_count_{ 0 };
        };

        class wait_group_cleanup
        {
        public:
            explicit wait_group_cleanup( wait_group& wg ) : wg_{ wg }
            {
            }
            ~wait_group_cleanup( )
            {
                wg_.done( );
            }

        private:
            wait_group& wg_;
        };

    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_PRIVATE_WAIT_GROUP_HH
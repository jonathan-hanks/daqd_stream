//
// Created by jonathan.hanks on 2/15/22.
//

#ifndef DAQD_STREAM_MULTI_SOURCE_INFO_BLOCK_HH
#define DAQD_STREAM_MULTI_SOURCE_INFO_BLOCK_HH

#include <atomic>
#include <cstdint>
#include <string>
#include <vector>

#include <daqd_stream/fixed_string.hh>
#include "signaling.hh"

namespace daqd_stream
{
    namespace detail
    {
        constexpr int multi_source_magic = 0x44335566;
        constexpr int multi_source_max_sources = 1024;
        constexpr int multi_source_max_source_length = 100;
        using multi_source_name =
            fixed_string< multi_source_max_source_length >;

        struct multi_source_info_block
        {
            std::int32_t         magic;
            std::int32_t         version;
            signaling_data_block signal_block;
            std::uint64_t        block_range;
            multi_source_name    sources[ multi_source_max_sources ];
        };
        static_assert( sizeof( multi_source_info_block ) == 102424,
                       "Incorrect size of the multi source info block" );

        inline std::uint64_t
        encode_block_range( std::uint32_t start, std::uint32_t end )
        {
            return ( static_cast< std::uint64_t >( start ) << 32 ) |
                static_cast< std::uint64_t >( end );
        }

        inline std::uint32_t
        decode_block_range_start( std::uint64_t block_range )
        {
            return static_cast< std::uint32_t >( block_range >> 32 );
        }

        inline std::uint32_t
        decode_block_range_end( std::uint64_t block_range )
        {
            return static_cast< std::uint32_t >( block_range & 0x0ffffffff );
        }

        inline void
        initialize_source_info_block( multi_source_info_block* block )
        {
            block->magic = multi_source_magic;
            block->version = 0;
            block->signal_block.futex_data_16th = 0;
            block->signal_block.futex_data_sec = 0;
            auto cur_start = decode_block_range_start( block->block_range );
            auto cur_end = decode_block_range_end( block->block_range );
            if ( cur_end >= multi_source_max_sources ||
                 cur_start >= multi_source_max_sources )
            {
                block->block_range = encode_block_range( 0, 0 );
            }
            else
            {
                block->block_range = encode_block_range( cur_end, cur_end );
            }
        }

        inline std::vector< std::string >
        get_configured_source_names(
            const volatile multi_source_info_block* info )
        {
            std::vector< std::string > sources{ };
            // std::uint64_t range = *(reinterpret_cast<std::atomic<const
            // volatile std::uint64_t>*>(&info->block_range));
            std::uint64_t range =
                *(std::atomic< std::uint64_t >*)( &( info->block_range ) );
            auto cur = decode_block_range_start( range );
            auto end = decode_block_range_end( range );
            if ( cur < multi_source_max_sources &&
                 end < multi_source_max_sources && info->version == 0 &&
                 info->magic == multi_source_magic )
            {
                while ( cur != end )
                {
                    if ( strlen(
                             (const char*)( info->sources[ cur ].data( ) ) ) >
                         info->sources[ cur ].capacity( ) )
                    {
                        break;
                    }
                    sources.emplace_back(
                        (const char*)info->sources[ cur ].data( ) );
                    cur = ( cur + 1 ) % multi_source_max_sources;
                }
            }
            return sources;
        }
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_MULTI_SOURCE_INFO_BLOCK_HH

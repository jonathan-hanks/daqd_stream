//
// Created by jonathan.hanks on 10/30/21.
//

#ifndef DAQD_STREAM_PREDICATES_HH
#define DAQD_STREAM_PREDICATES_HH

#include <daqd_stream/online_channel.hh>

namespace daqd_stream
{
    namespace detail
    {

        /**
         * @brief less_by_name is a predicate function for searching/sorting
         * online_channels by name
         * @param a
         * @param b
         * @return true if a.name < b.name
         */
        inline bool
        less_by_name( const online_channel& a, const online_channel& b )
        {
            return a.name < b.name;
        }

        /**
         * @brief search_by_name is a comparison functor used for searching on
         * online_channels.
         * @note this is written as fixed_string is not aware of std::string
         */
        struct search_by_name
        {
            bool
            operator( )( const std::string& a, const online_channel& b )
            {
                return std::strcmp( a.c_str( ), b.name.data( ) ) < 0;
            }
            bool
            operator( )( const online_channel& a, const std::string& b )
            {
                return std::strcmp( a.name.data( ), b.c_str( ) ) < 0;
            }
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_PREDICATES_HH

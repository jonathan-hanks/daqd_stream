/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_CHECKSUM_HH
#define DAQD_STREAM_CHECKSUM_HH

#include <utility>
#include <daqd_stream/online_channel.hh>

namespace daqd_stream
{
    namespace detail
    {
        /*!
         * @brief A class to manage the genrate of a checksum
         * @note implemented as a crc32
         */
        class checksum_generator
        {
        public:
            checksum_generator( ) : sum_{ 0 }, len_consumed_{ 0 }
            {
            }
            checksum_generator( const checksum_generator& other ) = default;
            checksum_generator&
            operator=( const checksum_generator& other ) = default;

            /*!
             * @brief consume the given channel in the hash
             * @param channel input channel
             */
            void hash( const online_channel& channel );
            /*!
             * @brief consume a binary blob
             * @param data start of data
             * @param size length
             */
            void hash( const char* data, std::size_t size );

            /*!
             * @breif reset/clear the hash
             */
            void
            reset( )
            {
                sum_ = 0;
                len_consumed_ = 0;
            }

            /*!
             * @brief Return the checksum of the data up to this point.
             * @note the checksum object is still usable and may consume more
             * input and provide updated checksums.
             * @return A checksum value
             */
            checksum_t get_checksum( ) const;

        private:
            checksum_t  sum_;
            std::size_t len_consumed_;
        };

        template < typename T >
        struct identity
        {
            const T&
            operator( )( const T& t ) const noexcept
            {
                return t;
            }
            T&&
            operator( )( T&& t ) const noexcept
            {
                return std::forward< T >( t );
            }
        };

        template < typename ConfigContainer,
                   typename TransformType =
                       identity< typename ConfigContainer::value_type > >
        checksum_t
        calculate_master_config( const ConfigContainer& dcu_config )
        {
            checksum_generator gen;
            TransformType      t;
            for ( const auto& input_sum : dcu_config )
            {
                auto sum = t( input_sum );
                gen.hash( reinterpret_cast< const char* >( &sum ),
                          sizeof( sum ) );
            }
            return gen.get_checksum( );
        }

    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_CHECKSUM_HH

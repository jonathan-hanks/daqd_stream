/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_CONSTANTS_HH
#define DAQD_STREAM_CONSTANTS_HH

#include <cstdlib>

namespace daqd_stream
{
    namespace detail
    {
        inline constexpr std::size_t
                         mb_to_bytes( const std::size_t mb ) noexcept
        {
            return mb * 1024 * 1024;
        }

        /*!
         * @return Maximum DCU id we can handle
         */
        inline constexpr int
        max_dcu( ) noexcept
        {
            return 256;
        }

        /*!
         * @return the cycles in a second.
         */
        inline constexpr int
        cycles_per_sec( ) noexcept
        {
            return 16;
        }

        inline constexpr int
        max_data_blocks( int lookback_sec ) noexcept
        {
            return lookback_sec * cycles_per_sec( );
        }
    } // namespace detail
} // namespace daqd_stream
#endif // DAQD_STREAM_CONSTANTS_HH

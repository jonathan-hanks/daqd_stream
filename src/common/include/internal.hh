/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_INTERNAL_HH
#define DAQD_STREAM_INTERNAL_HH

#include <array>
#include <iostream>
#include <set>
#include <string>

#include <cds-pubsub/sub.hh>

#include <daqd_stream/basic_types.hh>
#include <daqd_stream/time_point.hh>

#include "constants.hh"
#include "concatenate.hh"
#include "data_types.hh"
#include "shared_structures.hh"
#include "offset_ptr.hh"
#include "posix_shmem.hh"
#include "predicates.hh"
#include "shared_types.hh"
#include "signaling.hh"

namespace daqd_stream
{
    namespace detail
    {

        /**
         * @brief calculate the number of bytes in a 1/16th data block for a
         * given datatype/rate
         * @param dt the data type to get size information regarding
         * @param datarate the datarate (must be divisible by 16)
         * @return size, 0 on error
         */
        static inline int
        calculate_bytes_per_16th( DATA_TYPE dt, int datarate )
        {
            return ( data_type_size( dt ) * datarate ) / 16;
        }

        /**
         * @brief Used to note the newest data and the accessible buffer of a
         * source
         */
        class avail_info
        {
            std::uint64_t end_val{ 0 };
            std::uint64_t start_val{ 0 };

        public:
            static avail_info
            from_time_values( std::uint64_t end_value,
                              std::uint64_t start_value )
            {
                avail_info info;
                info.end_val = end_value;
                info.start_val = start_value;
                return info;
            }

            avail_info( ) = default;
            avail_info( std::uint64_t gps_end,
                        std::uint64_t cycle_end,
                        std::uint64_t gps_start,
                        std::uint64_t cycle_start )
                : end_val{ ( gps_end << 8 ) | cycle_end }, start_val{
                      ( gps_start << 8 ) | cycle_start
                  }
            {
            }

            avail_info( std::uint64_t gps_end,
                        std::uint64_t cycle_end,
                        std::uint64_t buffer_size_sec )
                : end_val{ ( gps_end << 8 ) | cycle_end }, start_val{
                      ( ( gps_end - buffer_size_sec ) << 8 ) | cycle_end
                  }
            {
            }
            avail_info( const avail_info& other ) noexcept = default;
            avail_info& operator=( const avail_info& other ) noexcept = default;

            // helpers
            std::uint64_t
            end_gps( ) const noexcept
            {
                return end_val >> 8;
            }
            std::uint64_t
            start_gps( ) const noexcept
            {
                return start_val >> 8;
            }
            std::uint64_t
            end_cycle( ) const noexcept
            {
                return end_val & 0x0ff;
            }
            std::uint64_t
            start_cycle( ) const noexcept
            {
                return start_val & 0x0ff;
            }

            bool
            operator==( const avail_info& other ) const noexcept
            {
                return start_val == other.start_val && end_val == other.end_val;
            }

            std::uint64_t
            end_time_value( ) const noexcept
            {
                return end_val;
            }
            std::uint64_t
            start_time_value( ) const noexcept
            {
                return start_val;
            }
        };

        inline bool
        overlaps( const avail_info& a1, const avail_info& a2 ) noexcept
        {
            return !( a1.end_time_value( ) <= a2.start_time_value( ) ||
                      a2.end_time_value( ) <= a1.start_time_value( ) );
        }

        inline avail_info
        intersection( const avail_info& a1, const avail_info& a2 ) noexcept
        {
            auto start =
                std::max( a1.start_time_value( ), a2.start_time_value( ) );
            auto end = std::min( a1.end_time_value( ), a2.end_time_value( ) );
            return avail_info::from_time_values( end, start );
        }

        /*!
         * @brief data and info for a cycles's worth of data
         */
        struct data_block
        {
            std::uint64_t gps_second;
            std::uint64_t cycle;

            /*!
             * @brief the offset into data of each dcu
             */
            std::array< std::size_t, max_dcu( ) > dcu_offsets;
            /*!
             * @brief the status of each dcu
             */
            std::array< uint32_t, max_dcu( ) > dcu_status;
            /*!
             * @brief the config checksum of each dcu
             */
            std::array< checksum_t, max_dcu( ) > dcu_config;
            /*!
             * @brief the data
             */
            char data[ mb_to_bytes( 100 ) / 16 ];
        };

        using shared_ifo_config =
            ifo_config_base< shared_span< online_channel > >;

        static inline std::int64_t
        validate_shmem_size( std::int64_t  total_size,
                             std::uint32_t sec_in_buffer )
        {
            std::int64_t req_size =
                ( 1 +
                  static_cast< std::int64_t >( sec_in_buffer ) *
                      static_cast< std::int64_t >( cycles_per_sec( ) ) ) *
                static_cast< std::int64_t >( sizeof( data_block ) );
            if ( total_size < req_size )
            {
                throw std::runtime_error(
                    "The shared memory buffer is too small for the lookback" );
            }
            return total_size;
        }

        /*!
         * @brief main coordinating structure for the shared memory block
         */
        struct shared_mem_header
        {
            shared_mem_header( std::int64_t  seg_size,
                               std::uint32_t lookback_sec = 5 )
                : cur_block{ ::daqd_stream::detail::max_data_blocks(
                                 lookback_sec ) -
                             1 },
                  num_blocks{ static_cast< std::uint32_t >(
                      ::daqd_stream::detail::max_data_blocks(
                          lookback_sec ) ) },
                  segment_size{ validate_shmem_size( seg_size, lookback_sec ) },
                  bump_{ nullptr }
            {
                bump_ = data_begin( );
            }
            std::uint32_t                   magic_bytes{ 0xda90da7a };
            std::uint32_t                   struct_id{ 3 };
            std::uint32_t                   version{ 1 };
            std::uint32_t                   padding_{ 0 };
            std::int32_t                    cur_block;
            std::uint32_t                   num_blocks;
            std::int64_t                    segment_size;
            offset_ptr< char >              bump_{ nullptr };
            signaling_data_block            signal_block{ };
            offset_ptr< shared_ifo_config > config_{ nullptr };
            // v0 used a fixed size buffer
            // for v1 look at using a run time sized
            // std::array< data_block, max_data_blocks( ) > blocks;

            /*!
             * @brief The number of 16Hz data blocks
             * @return The number of 16Hz data blocks
             */
            int
            max_data_blocks( ) const
            {
                return static_cast< int >( num_blocks );
            }

            /*!
             * @brief the number of blocks that reads can start in from the
             * current
             * block
             * @return
             */
            int
            safe_data_blocks( ) const noexcept
            {
                return static_cast< int >( num_blocks ) - cycles_per_sec( );
            }

            /*!
             * @brief the number of seconds that reads can start
             */
            int
            safe_data_seconds( ) const noexcept
            {
                return static_cast< int >(
                           ( num_blocks / cycles_per_sec( ) ) ) -
                    1;
            }

            data_block*
            blocks_begin( )
            {
                return reinterpret_cast< data_block* >( this + 1 );
            }

            data_block*
            blocks_end( )
            {
                return blocks_begin( ) + num_blocks;
            }

            data_block&
            blocks( int index )
            {
                return blocks_begin( )[ index ];
            }

            const data_block*
            blocks_begin( ) const noexcept
            {
                return reinterpret_cast< const data_block* >( this + 1 );
            }

            const data_block*
            blocks_end( ) const noexcept
            {
                return blocks_begin( ) + num_blocks;
            }

            const data_block&
            blocks( int index ) const
            {
                return blocks_begin( )[ index ];
            }

            std::int32_t
            get_cur_block( ) const noexcept
            {
                return reinterpret_cast< const std::atomic< std::int32_t >* >(
                           &cur_block )
                    ->load( );
            }

            void
            set_cur_block( std::int32_t new_cur_block ) noexcept
            {
                reinterpret_cast< std::atomic< std::int32_t >* >( &cur_block )
                    ->store( new_cur_block );
            }

            int
            prev_block( ) const
            {
                auto cur = get_cur_block( );
                return ( cur > 0 ? cur - 1 : max_data_blocks( ) - 1 );
            }
            int
            prev_block( int index ) const noexcept
            {
                return ( index > 0 ? index - 1 : max_data_blocks( ) - 1 );
            }

            int
            next_block( int index ) const noexcept
            {
                return ( index + 1 ) % max_data_blocks( );
            }
            int
            next_block( ) const
            {
                return next_block( get_cur_block( ) );
            }
            void
            advance_block_counter( )
            {
                set_cur_block( next_block( ) );
            }

            shared_ifo_config*
            get_config( ) const
            {
                return config_.get( );
            }

            void
            set_config( shared_ifo_config* cfg )
            {
                config_ = cfg;
            }

            const char*
            data_begin( ) const
            {
                return reinterpret_cast< const char* >( blocks_end( ) );
            }

            char*
            data_begin( )
            {
                return reinterpret_cast< char* >( blocks_end( ) );
            }

            const char*
            data_end( ) const
            {
                return reinterpret_cast< const char* >( &magic_bytes ) +
                    segment_size;
                // return ( data_begin( ) - sizeof( *this ) ) + segment_size;
            }

            char*
            data_end( )
            {
                return reinterpret_cast< char* >( &magic_bytes ) + segment_size;
                // return ( data_begin( ) - sizeof( *this ) ) + segment_size;
            }

            char*
            get_mem( std::size_t s )
            {
                auto cur = bump_.get( );
                auto end = data_end( );
                if ( end - cur > s )
                {
                    bump_ += s;
                    return cur;
                }
                throw std::bad_alloc( );
            }

            template < typename T, typename... Args >
            T*
            construct( Args&&... args )
            {
                void* raw = get_mem( sizeof( T ) );
                return new ( raw ) T( std::forward< Args >( args )... );
            }

            size_t
            remaining_memory( ) const
            {
                auto cur = bump_.get( );
                auto end = data_end( );
                return end - cur;
            }

            void
            reset_dynamic_ram( )
            {
                bump_ = data_begin( );
            }
        };
        static_assert( offsetof( shared_mem_header, magic_bytes ) == 0,
                       "magic_bytes is the first entry" );
        static_assert( offsetof( shared_mem_header, struct_id ) == 4,
                       "struct_id is the second entry" );
        static_assert( offsetof( shared_mem_header, version ) == 8,
                       "version is the third entry" );
        static_assert( offsetof( shared_mem_header, cur_block ) == 16,
                       "cur_block is at offset 16" );
        static_assert( offsetof( shared_mem_header, num_blocks ) == 20,
                       "num_blocks is at offset 20" );
        static_assert( offsetof( shared_mem_header, segment_size ) == 24,
                       "segment_size is at offset 24" );
        static_assert( offsetof( shared_mem_header, signal_block ) == 40,
                       "the signaling block is at offset 24" );
        static_assert( sizeof( offset_ptr< shared_ifo_config > ) == 8,
                       "offset pointers should be 64bit" );
        static_assert( offsetof( shared_mem_header, config_ ) == 48,
                       "the config is at offset 32" );

        constexpr inline std::uint64_t
        nano_in_cycle( ) noexcept
        {
            return 1000000000 / 16;
        }

        constexpr inline std::uint64_t
        cycle_to_nano( std::uint64_t cycle ) noexcept
        {
            return cycle * nano_in_cycle( );
        }

        constexpr inline std::uint64_t
        nano_to_cycle( std::uint64_t nano ) noexcept
        {
            return ( nano % 1000000000 ) / 62500000;
        }

        constexpr inline time_point
        time_point_from_cycle( std::uint64_t seconds,
                               std::uint64_t cycle ) noexcept
        {
            return make_time_point( seconds, cycle_to_nano( cycle ) );
        }

        inline std::int32_t
        get_next_block_index_16th( const shared_mem_header* header,
                                   const time_point& last_time_seen ) noexcept
        {
            auto index = header->cur_block;
            if ( last_time_seen.seconds == 0 )
            {
                return index;
            }

            auto tries = header->safe_data_blocks( );
            auto cur = header->get_cur_block( );

            while ( tries > 0 )
            {
                const data_block& cur_block = header->blocks( cur );
                auto cur_time = time_point_from_cycle( cur_block.gps_second,
                                                       cur_block.cycle );
                if ( cur_time > last_time_seen )
                {
                    index = cur;
                }
                --tries;
                cur = header->prev_block( cur );
            }
            return index;
        }

        /*!
         * @brief create the 'main'/'server' memory segment
         * @param segment_name name of the segment
         * @param segment_size size
         * @return the memory block object
         */
        inline std::shared_ptr< Posix_shared_memory >
        create_shared_memory( const std::string& segment_name,
                              std::size_t        segment_size,
                              int                permissions )
        {
            // Posix_shared_memory::remove( segment_name );

            return std::make_shared< Posix_shared_memory >(
                segment_name, segment_size, permissions );
        }

    } // namespace detail
} // namespace daqd_stream
#endif // DAQD_STREAM_INTERNAL_HH

/*
Copyright 2021 California Institute of Technology.

You should have received a copy of the licensing terms for this software
included in the file “LICENSE” located in the top-leveldirectory of this
package.If you did not, you can view a copy at
http://dcc.ligo.org/M1500244/LICENSE.txt
*/
#ifndef DAQD_STREAM_OFFSET_PTR_HH
#define DAQD_STREAM_OFFSET_PTR_HH

#include <cstdint>

//#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/offset_ptr.hpp>
//#include <boost/interprocess/sync/interprocess_condition.hpp>
//#include <boost/interprocess/sync/interprocess_mutex.hpp>
//#include <boost/interprocess/sync/scoped_lock.hpp>
// namespace interprocess = boost::interprocess;
template < typename T >
using offset_ptr = boost::interprocess::offset_ptr< T >;
// using offset_ptr = boost::interprocess::offset_ptr<T, std::int64_t,
// std::uint64_t, sizeof(std::int64_t)>;

// using managed_shared_memory = interprocess::basic_managed_shared_memory<char,
// interprocess::rbtree_best_fit<interprocess::null_mutex_family>,
//    interprocess::iset_index>;

#endif // DAQD_STREAM_OFFSET_PTR_HH

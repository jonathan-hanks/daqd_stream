//
// Created by jonathan.hanks on 9/21/21.
//
#include <daqd_stream/plugins/sub_frame_plugin.hh>
#include "frame_plugin_internal.hh"

#include <advligorts/daq_metadata.h>

#include <atomic>
#include <chrono>
#include <cstdint>
#include <stdexcept>
#include <thread>

#include <boost/filesystem.hpp>

namespace daqd_stream
{
    namespace plugins
    {
        namespace frame_sub
        {
            class FlatFrameSubscription : public pub_sub::plugins::Subscription
            {
            public:
                FlatFrameSubscription(
                    const std::string&             frame_type,
                    const boost::filesystem::path& frame_path,
                    pub_sub::SubHandler&           handler )
                    : pub_sub::plugins::Subscription( ),
                      frame_type_{ frame_type },
                      frame_path_{ frame_path }, handler_{ handler },
                      worker_( &FlatFrameSubscription::main_loop, this )
                {
                }
                ~FlatFrameSubscription( ) override
                {
                    done_ = true;
                    worker_.join( );
                };

            private:
                info_and_path
                find_next_frame( std::int64_t last_seen )
                {
                    return daqd_stream::plugins::frame_sub::find_next_frame(last_seen, frame_path_, frame_type_);
                }

                void
                main_loop( )
                {
                    std::int64_t latest_gps{ 0 };
                    while ( !done_ )
                    {
                        auto info = find_next_frame( latest_gps );
                        if ( info.info.gps_start > latest_gps )
                        {
                            try
                            {
                                process_frame( info, handler_, sub_id( ) );
                                latest_gps = info.info.gps_start;
                            }
                            catch(...)
                            {
                            }
                        }
                        std::this_thread::sleep_for(
                            std::chrono::milliseconds( 250 ) );
                    }
                }

                std::atomic< bool >     done_{ false };
                std::string             frame_type_;
                boost::filesystem::path frame_path_;
                std::uint64_t           last_seen_gps_{ 0 };
                pub_sub::SubHandler     handler_;
                // this should be the last entry
                std::thread worker_;
            };

            class FlatFrameSubPlugin
                : public pub_sub::plugins::SubscriptionPluginApi
            {
            public:
                ~FlatFrameSubPlugin( ) override = default;
                const std::string&
                prefix( ) const override
                {
                    static const std::string my_prefix{ "gwf://" };
                    return my_prefix;
                }
                const std::string&
                version( ) const override
                {
                    static const std::string my_version{ "0" };
                    return my_version;
                }
                const std::string&
                name( ) const override
                {
                    static const std::string my_name{ "Flat frame source" };
                    return my_name;
                }
                bool
                is_source( ) const override
                {
                    return true;
                }
                std::shared_ptr< pub_sub::plugins::Subscription >
                subscribe( const std::string&        conn_str,
                           pub_sub::SubDebugNotices& debug_hooks,
                           pub_sub::SubHandler       handler ) override
                {
                    std::string handle_path =
                        conn_str.substr( prefix( ).size( ) );
                    auto idx = handle_path.find( ':' );
                    if ( idx == std::string::npos || idx == 0 )
                    {
                        throw std::runtime_error(
                            "Frame sources must have the format "
                            "gwf://type:dcuid:path" );
                    }
                    auto frame_type = handle_path.substr( 0, idx );
                    auto frame_path = handle_path.substr( idx + 1 );
                    return std::make_shared< FlatFrameSubscription >(
                        frame_type, frame_path, handler );
                }
            };

        } // namespace frame_sub

        std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
        get_frame_sub_plugin( )
        {
            return std::make_shared< frame_sub::FlatFrameSubPlugin >( );
        }
    } // namespace plugins
} // namespace daqd_stream
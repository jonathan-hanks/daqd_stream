//
// Created by jonathan.hanks on 9/21/21.
//

#ifndef DAQD_STREAM_SUB_FRAME_PLUGIN_HH
#define DAQD_STREAM_SUB_FRAME_PLUGIN_HH

#include <cds-pubsub/sub_plugin.hh>

namespace daqd_stream
{
    namespace plugins
    {
        std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
        get_frame_sub_plugin( );
    }
} // namespace daqd_stream
#endif // DAQD_STREAM_SUB_FRAME_PLUGIN_HH

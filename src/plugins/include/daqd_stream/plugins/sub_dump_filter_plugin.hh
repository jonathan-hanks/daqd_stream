//
// Created by jonathan.hanks on 10/3/22.
//

#ifndef DAQD_STREAM_SUB_DUMP_FILTER_HH
#define DAQD_STREAM_SUB_DUMP_FILTER_HH

#include <cds-pubsub/sub_plugin.hh>

namespace plugins
{
    std::shared_ptr< pub_sub::plugins::SubscriptionPluginApi >
    get_dump_filter_sub_plugin( );
}

#endif // DAQD_STREAM_SUB_DUMP_FILTER_HH

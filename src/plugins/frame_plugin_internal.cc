//
// Created by jonathan.hanks on 9/23/21.
//
#include "frame_plugin_internal.hh"

#include <array>
#include <memory>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/utility/string_view.hpp>

#include "framecpp/FrameH.hh"
#include "framecpp/FrVect.hh"
// #include "framecpp/Dimension.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/IFrameStream.hh"
#include "ldastoolsal/fstream.hh"

#include <daqd_stream/daqd_stream.hh>

#include <advligorts/daq_core.h>
#include <advligorts/daq_metadata.h>

#include "crc.h"

#include <iomanip>

namespace daqd_stream
{
    namespace plugins
    {
        namespace frame_sub
        {
            boost::optional< frame_info >
            parse_frame_name( const std::string& name )
            {
                static const boost::string_view GWF{ ".gwf" };

                if ( !boost::algorithm::ends_with( name, GWF ) )
                {
                    return boost::none;
                }
                boost::string_view input( name.c_str( ),
                                          name.size( ) - GWF.size( ) );

                int type_sep{ 0 };
                int time_sep{ 0 };
                for ( int cur = 0; cur != boost::string_view::npos;
                      cur = input.find( '-', cur + 1 ) )
                {
                    type_sep = time_sep;
                    time_sep = cur;
                }
                if ( type_sep == 0 || ( time_sep == type_sep + 1 ) )
                {
                    return boost::none;
                }
                auto duration_view = input.substr( time_sep + 1 );
                auto time_view =
                    input.substr( type_sep + 1, ( time_sep - type_sep ) - 1 );
                frame_info info{ };
                auto       type_view = input.substr( 0, type_sep );
                info.frame_type =
                    std::string( type_view.data( ), type_view.size( ) );
                info.gps_start = to_pos_int< std::int64_t >( time_view );
                auto duration = to_pos_int< std::int64_t >( duration_view );
                info.gps_stop = info.gps_start + duration;
                if ( info.gps_start == 0 || duration == 0 )
                {
                    return boost::none;
                }
                return info;
            }

            template < typename T >
            std::shared_ptr< T[] >
            make_shared_array( std::size_t count )
            {
                return std::shared_ptr< unsigned char[] >( new T[ count ],
                                                           []( T* p ) {
                                                               if ( p )
                                                               {
                                                                   delete[] p;
                                                               }
                                                           } );
            }

            DATA_TYPE
            fcpp_to_data_type( int fcpp )
            {
                using DT = DATA_TYPE;
                switch ( fcpp )
                {
                case FrameCPP::FrVect::FR_VECT_2S:
                    return DT::INT16;
                case FrameCPP::FrVect::FR_VECT_4S:
                    return DT::INT32;
                case FrameCPP::FrVect::FR_VECT_8S:
                    return DT::INT64;
                case FrameCPP::FrVect::FR_VECT_4R:
                    return DT::FLOAT32;
                case FrameCPP::FrVect::FR_VECT_8R:
                    return DT::FLOAT64;
                case FrameCPP::FrVect::FR_VECT_8C:
                    return DT::COMPLEX32;
                case FrameCPP::FrVect::FR_VECT_4U:
                    return DT::UINT32;
                default:
                    return DT::UNDEFINED;
                }
            }

            void
            process_frame( const info_and_path& info,
                           pub_sub::SubHandler& handler,
                           pub_sub::SubId       sub_id )
            {
                using vect_ptr = boost::shared_ptr< FrameCPP::FrVect >;

                std::string             path = info.path.string( );
                FrameCPP::IFrameFStream f_stream( path.c_str( ) );
                auto                    toc = f_stream.GetTOC( );
                if ( toc->GetNFrame( ) == 0 )
                {
                    throw std::runtime_error( "no frames in .gwf file" );
                }
                // auto frame = f_stream.ReadFrameH(0, true);
                auto  frame = f_stream.ReadNextFrame( );
                auto  raw = frame->GetRawData( );
                auto& proc_ref = frame->RefProcData( );

                int raw_size = 0;
                int reduced_size = static_cast< int >( proc_ref.size( ) );
                if ( raw )
                {
                    raw_size =
                        static_cast< int >( ( *raw ).RefFirstAdc( ).size( ) );
                }
                auto dest_size = raw_size + reduced_size;
                std::unique_ptr< ::daqd_stream::online_channel[] > dest{
                    new ::daqd_stream::online_channel[ dest_size ]
                };
                auto dest_end = dest.get( ) + dest_size;
                auto dest_cur = dest.get( );

                std::size_t cur_offset = 0;

                std::vector< vect_ptr > data_vec{ };
                data_vec.reserve( dest_size );

                if ( raw )
                {
                    for ( const auto& adc : raw->RefFirstAdc( ) )
                    {
                        auto& name = adc->GetName( );
                        auto  raw_rate = adc->GetSampleRate( );
                        auto  rate = static_cast< int >( raw_rate );
                        if ( raw_rate < 16 || rate % 16 != 0 )
                        {
                            continue;
                        }
                        vect_ptr data = adc->RefData( ).front( );
                        auto data_type = fcpp_to_data_type( data->GetType( ) );
                        auto index = name.find( '.' );
                        if ( index != std::string::npos || name.size( ) >= 60 )
                        {
                            continue;
                        }
                        if ( dest_cur < dest_end )
                        {
                            dest_cur->name = name.c_str( );
                            dest_cur->dcuid = 1;
                            dest_cur->datatype = data_type;
                            dest_cur->datarate = rate;
                            dest_cur->bytes_per_16th =
                                detail::data_type_size( data_type ) *
                                ( rate / 16 );
                            dest_cur->data_offset = cur_offset;
                            dest_cur->signal_gain = 1.0;
                            dest_cur->signal_slope = 1.0;
                            dest_cur->signal_offset = 0.0;

                            cur_offset += dest_cur->bytes_per_16th;
                            ++dest_cur;

                            data_vec.emplace_back( std::move( data ) );
                        }
                        else
                        {
                            throw std::runtime_error(
                                "unexpected channel found" );
                        }
                    }
                }
                for ( const auto& proc : proc_ref )
                {
                    auto&    name = proc->GetName( );
                    vect_ptr data = proc->RefData( ).front( );
                    auto     raw_rate = 1.0 / data->GetDim( 0 ).GetDx( );
                    auto     rate = static_cast< int >( raw_rate );
                    if ( raw_rate < 16 || rate % 16 != 0 )
                    {
                        continue;
                    }

                    auto data_type = fcpp_to_data_type( data->GetType( ) );
                    if ( dest_cur < dest_end )
                    {
                        dest_cur->name = name.c_str( );
                        dest_cur->dcuid = 1;
                        dest_cur->datatype = data_type;
                        dest_cur->datarate = rate;
                        dest_cur->bytes_per_16th =
                            detail::data_type_size( data_type ) * ( rate / 16 );
                        dest_cur->data_offset = cur_offset;
                        dest_cur->signal_gain = 1.0;
                        dest_cur->signal_slope = 1.0;
                        dest_cur->signal_offset = 0.0;

                        cur_offset += dest_cur->bytes_per_16th;
                        ++dest_cur;

                        data_vec.emplace_back( std::move( data ) );
                    }
                    else
                    {
                        throw std::runtime_error( "unexpected channel found" );
                    }
                }

                std::array< std::shared_ptr< unsigned char[] >, 16 > blobs{ };
                std::array< daq_dc_data_t*, 16 > data_headers{ };
                std::array< unsigned char*, 16 > data_blocks{ };

                for ( auto i = 0; i < 16; ++i )
                {
                    blobs[ i ] = make_shared_array< unsigned char >(
                        sizeof( daq_dc_data_t ) );
                    data_headers[ i ] =
                        reinterpret_cast< daq_dc_data_t* >( blobs[ i ].get( ) );
                    data_blocks[ i ] = reinterpret_cast< unsigned char* >(
                        data_headers[ i ]->dataBlock );
                }
                unsigned int config_checksum = 0;
                {
                    auto         actual_channels = dest_end - dest.get( );
                    unsigned int dcu_header_size =
                        sizeof( daq_dcu_metadata_header_t );
                    unsigned int msg_header_size =
                        sizeof( daq_multi_dcu_metadata_header_t );
                    unsigned int metadata_size =
                        sizeof( daq_channel_metadata_t ) * actual_channels;
                    auto message_size =
                        msg_header_size + dcu_header_size + metadata_size;

                    auto data_ptr =
                        make_shared_array< unsigned char >( message_size );
                    auto* metadata =
                        reinterpret_cast< daq_multi_dcu_metadata_t* >(
                            data_ptr.get( ) );
                    metadata->header.magic = 0xffeeddcc;
                    metadata->header.version = 0;
                    metadata->header.dcuCount = 1;
                    metadata->header.dataBlockChecksum = 0;
                    // metadata->header.totalDataSize = dcu_header_size +
                    // metadata_size;

                    auto* dcu_header =
                        reinterpret_cast< daq_dcu_metadata_header_t* >(
                            metadata->data );
                    dcu_header->dcuId = 1;
                    dcu_header->numChannels = actual_channels;
                    dcu_header->checksum = 0;
                    dcu_header->encodingMethod = METADATA_PLAIN;
                    dcu_header->decodedSize = metadata_size;
                    dcu_header->encodedSize = metadata_size;
                    dcu_header->encodedChecksum = 0;

                    auto* channel_data =
                        reinterpret_cast< daq_channel_metadata_t* >(
                            &metadata->data[ sizeof(
                                daq_dcu_metadata_header_t ) ] );
                    std::transform(
                        dest.get( ),
                        dest_end,
                        channel_data,
                        []( const online_channel& input )
                            -> daq_channel_metadata_t {
                            daq_channel_metadata_t output;
                            bzero( output.name, sizeof( output.name ) );
                            bzero( output.units, sizeof( output.units ) );
                            strncpy( output.name,
                                     input.name.data( ),
                                     input.name.capacity( ) );
                            strncpy( output.units,
                                     input.units.data( ),
                                     input.units.capacity( ) );
                            output.datatype =
                                static_cast< int >( input.datatype );
                            output.datarate = input.datarate;
                            output.test_point_number = 0;
                            output.signal_gain = input.signal_gain;
                            output.signal_slope = input.signal_slope;
                            output.signal_offset = input.signal_offset;
                            return output;
                        } );

                    auto data_bytes =
                        actual_channels * sizeof( daq_channel_metadata_t );
                    config_checksum = dcu_header->checksum =
                        dcu_header->encodedChecksum = crc_len(
                            crc_ptr( const_cast< char* >(
                                         reinterpret_cast< const char* >(
                                             channel_data ) ),
                                     data_bytes,
                                     0 ),
                            data_bytes );

                    pub_sub::KeyType key{
                        ( frame->GetGTime( ).getSec( ) << 8 ) + 1
                    };

                    metadata->header.dataBlockSize =
                        data_bytes + sizeof( *dcu_header );

                    handler( pub_sub::SubMessage(
                        sub_id,
                        key,
                        pub_sub::Message( data_ptr, message_size ) ) );
                }

                auto data_cur = data_vec.begin( );
                for ( auto cur = dest.get( ); cur != dest_end; ++cur )
                {
                    auto offset = cur->data_offset;
                    auto stride = cur->bytes_per_16th;
                    auto raw_data_ptr = ( *data_cur )->GetDataUncompressed( );

                    auto* input = raw_data_ptr.get( );
                    for ( auto i = 0; i < 16; ++i )
                    {
                        auto* out = data_blocks[ i ] + offset;
                        std::copy( input, input + stride, out );
                        input += stride;
                    }
                    ++data_cur;
                }

                auto*        last_entry = ( dest_cur - 1 );
                unsigned int data_block_size =
                    last_entry->data_offset + last_entry->bytes_per_16th;
                std::size_t message_size =
                    data_block_size + sizeof( daq_multi_dcu_header_t );

                for ( auto i = 0; i < 16; ++i )
                {
                    daq_multi_dcu_header_t& header = data_headers[ i ]->header;
                    header.dcuTotalModels = 1;
                    header.fullDataBlockSize = data_block_size;
                    header.dcuheader[ 0 ].dcuId = 1;
                    header.dcuheader[ 0 ].fileCrc = config_checksum;
                    header.dcuheader[ 0 ].status = 0;
                    header.dcuheader[ 0 ].cycle = i;
                    header.dcuheader[ 0 ].timeSec =
                        frame->GetGTime( ).getSec( );
                    header.dcuheader[ 0 ].timeNSec = i * ( 1000000000 / 16 );
                    header.dcuheader[ 0 ].dataCrc = 0;
                    header.dcuheader[ 0 ].dataBlockSize = data_block_size;
                    header.dcuheader[ 0 ].tpBlockSize = 0;
                    header.dcuheader[ 0 ].tpCount = 0;

                    pub_sub::KeyType key{
                        ( header.dcuheader[ 0 ].timeSec << 8 ) + ( i << 1 )
                    };

                    handler( pub_sub::SubMessage(
                        sub_id,
                        key,
                        pub_sub::Message( blobs[ i ], message_size ) ) );
                }
            }

            info_and_path
            find_next_frame( std::int64_t last_seen, const boost::filesystem::path& frame_path, const std::string& frame_type )
            {
                info_and_path info{ };

                try
                {
                    boost::filesystem::directory_iterator cur(
                        frame_path );
                    boost::filesystem::directory_iterator end{ };
                    for ( ; cur != end; ++cur )
                    {
                        if ( !boost::filesystem::exists( cur->path( ) ) )
                        {
                            continue;
                        }
                        if ( !boost::filesystem::is_regular_file(
                                 cur->path( ) ) )
                        {
                            continue;
                        }
                        boost::filesystem::path filename{
                            cur->path( ).filename( )
                        };

                        boost::optional< frame_info > opt_info =
                            parse_frame_name( filename.string( ) );
                        if ( !static_cast< bool >( opt_info ) )
                        {
                            continue;
                        }
                        frame_info cur_info{ opt_info.get( ) };
                        if ( cur_info.frame_type != frame_type )
                        {
                            continue;
                        }
                        if ( last_seen == 0 )
                        {
                            if (cur_info.gps_start > info.info.gps_start)
                            {
                                info.info = std::move( cur_info );
                                info.path = cur->path( );
                            }
                        }
                        else if ( ( cur_info.gps_start > last_seen )
                                  && ( cur_info.gps_start < info.info.gps_start
                                       || info.info.gps_start == 0 ))
                        {
                            info.info = std::move( cur_info );
                            info.path = cur->path( );
                        }
                    }
                }
                catch ( ... )
                {
                }
                return info;
            }

        } // namespace frame_sub
    } // namespace plugins
} // namespace daqd_stream

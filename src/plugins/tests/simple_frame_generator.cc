//
// Created by jonathan.hanks on 2/1/22.
//
#include <atomic>
#include <chrono>
#include <csignal>
#include <cstdlib>
#include <deque>
#include <memory>
#include <string>
#include <stdexcept>
#include <thread>

#include "frame_utils.hh"
#include "gps.hh"

std::atomic< bool > done_{ false };

void
signal_handler( int dummy )
{
    done_ = true;
}

class argument_error : public std::runtime_error
{
public:
    argument_error( const char* msg ) : runtime_error( msg )
    {
    }
};

void
show_help( const char* prog_name, const char* msg )
{
    frame_utils::frame_generator_config default_opts{ };
    std::cout << "Usage:\n" << prog_name << " options\n";
    std::cout << "\nOptions:\n";
    std::cout << "\t-h|--help\t\tThis help\n";
    std::cout << "\t-r|--retain\t\tSeconds to retain frames ["
              << default_opts.retention_period_s << "]\n";
    std::cout
        << "\t-o|--output\t\tOutput directory, empty is a temp. directory ["
        << default_opts.frame_root << "]\n";
    std::cout << "\t-g|--gps\t\tUse /dev/gpstime as a clock\n";
    std::cout << "\t-v\t\t\tVerbose output\n";
}

frame_utils::frame_generator_config
parse_args( int argc, char* argv[], bool& use_gps )
{
    frame_utils::frame_generator_config cfg;
    std::deque< std::string >           args{ };
    for ( auto i = 1; i < argc; ++i )
    {
        args.emplace_back( argv[ i ] );
    }
    use_gps = false;
    auto pop = [ &args ]( ) -> std::string {
        std::string arg = args.front( );
        args.pop_front( );
        return arg;
    };
    auto pop_msg = [ &args, &pop ]( const char* msg ) -> std::string {
        if ( args.empty( ) )
        {
            throw argument_error( msg );
        }
        return pop( );
    };
    while ( !args.empty( ) )
    {
        auto arg = pop( );
        if ( arg == "-h" || arg == "--help" )
        {
            throw argument_error( "" );
        }
        else if ( arg == "-r" || arg == "--retain" )
        {
            auto val = pop_msg( "--retain|-r requires a argument" );
            cfg.retention_period_s = std::atoi( val.c_str( ) );
            if ( cfg.retention_period_s <= 1 )
            {
                throw argument_error(
                    "You must retain frames for more than 1s" );
            }
        }
        else if ( arg == "-o" || arg == "--output" )
        {
            cfg.frame_root = pop_msg( "--output|-o requires an argument" );
        }
        else if ( arg == "-g" || arg == "--gps" )
        {
            use_gps = true;
        }
        else if ( arg == "-v" )
        {
            cfg.verbose = true;
        }
        else
        {
            throw argument_error( "Unexpected argument" );
        }
    }
    return cfg;
}

int
main( int argc, char* argv[] )
{
    bool                                use_gps{ false };
    frame_utils::frame_generator_config cfg;
    try
    {
        cfg = parse_args( argc, argv, use_gps );
    }
    catch ( argument_error& error )
    {
        show_help( argv[ 0 ], error.what( ) );
        return 1;
    }
    std::unique_ptr< frame_utils::TempDir > temp_dir;
    if ( cfg.frame_root.empty( ) )
    {
        temp_dir = std::make_unique< frame_utils::TempDir >( );
        cfg.frame_root = temp_dir->get( ).string( );
        std::cout << "FrameDir: " << cfg.frame_root << std::endl;
    }

    std::signal( SIGINT, signal_handler );
    std::signal( SIGTERM, signal_handler );

    auto generators = frame_utils::create_sample_channels( 1024 );

    auto done_pred = []( ) -> bool { return done_; };
    if ( !use_gps )
    {
        simple_frame_loop( cfg, generators, done_pred );
    }
    else
    {
        GPS::gps_clock clock( 0 );
        frame_utils::simple_frame_loop_gps_clock(
            cfg, generators, clock, done_pred );
    }
    return 0;
}
//
// Created by jonathan.hanks on 2/1/22.
//

#ifndef DAQD_STREAM_FRAME_UTILS_HH
#define DAQD_STREAM_FRAME_UTILS_HH

#include <climits>
#include <chrono>
#include <cstdlib>
#include <deque>
#include <string>
#include <thread>

#include <boost/filesystem.hpp>

#include <framecpp/Common/CheckSum.hh>
#include <framecpp/Common/FrameBuffer.hh>
#include <framecpp/Common/FrameStream.hh>
#include <framecpp/Common/FrVect.hh>
#include <framecpp/FrameH.hh>
#include <framecpp/FrRawData.hh>

#include <daqd_stream/daqd_stream.hh>


namespace frame_utils
{
#if FRAMECPP_VERSION_NUMBER < 300000
    static inline auto get_frame_compression_type()
    {
        return FrameCPP::Version::FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP;
    }
#else
    static inline auto get_frame_compression_type()
    {
        return FrameCPP::Version::FrVect::GZIP;
    }
#endif


    using FrameHPtr = boost::shared_ptr< FrameCPP::Version::FrameH >;
    using DataPtr = std::pair< unsigned char*, INT_2U* >;

    using OffsetMap = std::map< unsigned int, std::size_t >;
    class SimpleGenerator
    {
        int
        pick_rate( int id )
        {
            switch ( id % 8 )
            {
            case 0:
                return 64 * 1024;
            case 1:
                return 32 * 1024;
            case 2:
            case 3:
            case 4:
                return 2048;
            default:
                return 16;
            }
        }

        std::size_t
        get_data_offset( OffsetMap& offsets, int dcuid )
        {
            return offsets[ dcuid ];
        }

        void
        increase_offset( OffsetMap& offsets, int dcuid, std::size_t amount )
        {
            offsets[ dcuid ] += amount;
        }

    public:
        SimpleGenerator( std::string name,
                         int         dcuid,
                         int         id,
                         OffsetMap&  offsets )
            : name_{ std::move( name ) }, rate_{ pick_rate( id ) },
              dcu_id_{ dcuid }, data_offset_{ get_data_offset( offsets,
                                                               dcuid ) },
              value_offset_{ id % ( 16 * 1024 ) }
        {
            increase_offset(
                offsets, dcuid, sizeof( std::int32_t ) * rate( ) / 16 );
        }

        int
        rate( ) const
        {
            return rate_;
        }
        std::size_t
        data_offset( ) const
        {
            return data_offset_;
        }
        std::size_t
        bytes_per_16th( ) const
        {
            return sizeof( std::int32_t ) * rate( ) / 16;
        }
        unsigned char*
        generate( unsigned char* dest, std::int64_t gps_s, int cycle ) const
        {
            auto* cur = reinterpret_cast< std::int32_t* >( dest );
            std::fill(
                cur,
                cur + ( rate( ) / 16 ),
                static_cast< std::int32_t >( gps_s << 4 | ( cycle & 0x0f ) ) );
            return dest + bytes_per_16th( );
        }

        daqd_stream::online_channel
        metadata( ) const
        {
            return daqd_stream::online_channel{
                name_.c_str( ),
                0,
                static_cast< unsigned char >( dcu_id_ ),
                daqd_stream::DATA_TYPE::INT32,
                rate( ),
                static_cast< int >( bytes_per_16th( ) ),
                data_offset_,
                1.0,
                1.0,
                0.0,
                "counts",
            };
        }

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

    private:
        std::string  name_;
        int          rate_;
        int          dcu_id_;
        std::size_t  data_offset_;
        std::int32_t value_offset_;
    };

    extern std::vector< SimpleGenerator >
    create_sample_channels( int         count,
                            std::string channel_prefix = "X1:CHAN-" );

    extern std::vector< daqd_stream::online_channel >
    to_channels( const std::vector< SimpleGenerator >& input );

    extern void
    generate_one_second( const std::vector< SimpleGenerator >& generators,
                         std::vector< DataPtr >&               outputs,
                         std::int64_t                          gps_s );

    extern void generate_16th( const std::vector< SimpleGenerator >& generators,
                               std::int64_t                          gps_s,
                               int                                   cycle,
                               unsigned char*                        dest,
                               std::size_t dest_size );

    class TempDir
    {
        static boost::filesystem::path
        get_temp_dir( )
        {
            std::string template_path =
                ( boost::filesystem::temp_directory_path( ) /
                  boost::filesystem::path( "tempdir-XXXXXX" ) )
                    .string( );
            std::vector< char > buffer;
            buffer.resize( template_path.size( ) + 1 );
            std::copy(
                template_path.begin( ), template_path.end( ), buffer.data( ) );
            buffer.back( ) = '\0';
            char* results = mkdtemp( buffer.data( ) );
            if ( results == nullptr )
            {
                throw std::runtime_error(
                    "Unable to create temporary directory" );
            }
            return boost::filesystem::path( buffer.data( ) );
        }

    public:
        TempDir( ) : path_( get_temp_dir( ) )
        {
        }
        ~TempDir( )
        {
            boost::system::error_code ec{ };
            boost::filesystem::remove_all( path_, ec );
        }

        TempDir( const TempDir& ) = delete;
        TempDir( TempDir&& ) = delete;

        TempDir& operator=( const TempDir& ) = delete;
        TempDir& operator=( TempDir&& ) = delete;

        const boost::filesystem::path&
        get( ) const
        {
            return path_;
        };

    private:
        boost::filesystem::path path_;
    };

    extern std::vector< DataPtr > AssociateDataPtrs(
        FrameCPP::Version::FrameH&                        frame,
        int                                               frame_period,
        const std::vector< daqd_stream::online_channel >& channels );

    extern FrameHPtr CreateFrameH( int frame_period );

    extern std::size_t data_size( daqd_stream::DATA_TYPE data_type );

    extern std::string create_path( const std::string& directory,
                                    const std::string& prefix,
                                    std::uint64_t      start,
                                    std::uint64_t      period );

    struct FrameData
    {
        FrameData( int                                        frame_period,
                   std::vector< daqd_stream::online_channel > channels )
            : frame_{ CreateFrameH( frame_period ) },
              channel_list_{ std::move( channels ) }, channel_pointers_{
                  AssociateDataPtrs( *frame_, frame_period, channel_list_ )
              }
        {
        }

        FrameData( int                                   frame_period,
                   const std::vector< SimpleGenerator >& channels )
            : frame_{ CreateFrameH( frame_period ) },
              channel_list_{ to_channels( channels ) }, channel_pointers_{
                  AssociateDataPtrs( *frame_, frame_period, channel_list_ )
              }
        {
        }

        void
        write( const std::string& directory,
               const std::string& prefix,
               std::uint64_t      start,
               std::uint64_t      period )
        {
            std::string path = create_path( directory, prefix, start, period );
            std::string tmp_path = path + ".tmp";
            auto        output_buf = std::make_unique<
                FrameCPP::Common::FrameBuffer< std::filebuf > >(
                std::ios::out );
            try
            {
                output_buf->open( tmp_path, std::ios::out | std::ios::binary );
                FrameCPP::Common::OFrameStream output_stream(
                    output_buf.release( ) );

                frame_->SetGTime( FrameCPP::Version::GPSTime( start, 0 ) );

                output_stream.WriteFrame(
                    frame_,
                    get_frame_compression_type(),
                    1,
                    FrameCPP::Common::CheckSum::CRC );
                output_stream.Close( );
                boost::filesystem::rename( tmp_path, path );
            }
            catch ( ... )
            {
                boost::system::error_code ec{ };
                boost::filesystem::remove( tmp_path, ec );
                throw;
            }
        }

        void
        write( const boost::filesystem::path& directory,
               const std::string&             prefix,
               std::uint64_t                  start,
               std::uint64_t                  period )
        {
            write( directory.string( ), prefix, start, period );
        }

        FrameHPtr                                  frame_{ nullptr };
        std::vector< daqd_stream::online_channel > channel_list_{ };
        std::vector< DataPtr >                     channel_pointers_{ };
    };

    template < typename TargetDataType >
    std::unique_ptr< FrameCPP::Version::FrVect >
    NewFrVect( const daqd_stream::online_channel& cur_channel,
               int                                frame_period )
    {
        INT_4U                        nx = cur_channel.datarate * frame_period;
        std::vector< TargetDataType > buffer( nx );
        FrameCPP::Version::Dimension dims[ 1 ] = { FrameCPP::Version::Dimension(
            nx, 1. / cur_channel.datarate, "time" ) };
        return std::make_unique< FrameCPP::Version::FrVect >(
            cur_channel.name.data( ),
            1,
            dims,
            buffer.data( ),
            cur_channel.units.data( ) );
    }

    struct frame_generator_config
    {
        std::string frame_root{ "" };
        int         retention_period_s{ 5 };
        bool        verbose{ false };
    };

    class frame_generator
    {
    public:
        explicit frame_generator( const frame_generator_config&   cfg,
                                  std::vector< SimpleGenerator >& generators )
            : cfg_{ cfg }, generators_{ generators }, fd_{ 1, generators_ }
        {
        }

        void
        generate( )
        {
            frame_utils::generate_one_second(
                generators_, fd_.channel_pointers_, sim_time_ );
            auto frame_path = frame_utils::create_path(
                cfg_.frame_root, "X1-TEST", sim_time_, 1 );
            fd_.write( cfg_.frame_root, "X1-TEST", sim_time_, 1 );
            if ( cfg_.verbose )
            {
                std::cerr << " " << frame_path << "\n";
            }
            frames_.push_back( frame_path );
            if ( frames_.size( ) > cfg_.retention_period_s )
            {
                boost::filesystem::remove(
                    boost::filesystem::path( frames_.front( ) ) );
                frames_.pop_front( );
            }
            std::this_thread::sleep_for( std::chrono::milliseconds( 255 ) );
            ++sim_time_;
        }

        /**
         * @brief Generate a corrupt frame, do not advance internal time
         * @note does not add to the frame retention list
         */
        void
        generate_corrupt( )
        {
            frame_utils::generate_one_second(
                generators_, fd_.channel_pointers_, sim_time_ );
            auto frame_path = frame_utils::create_path(
                cfg_.frame_root, "X1-TEST", sim_time_, 1 );
            fd_.write( cfg_.frame_root, "X1-TEST", sim_time_, 1 );
            auto size = boost::filesystem::file_size(frame_path);
            boost::filesystem::resize_file(frame_path, size/4);
            if ( cfg_.verbose )
            {
                std::cerr << " " << frame_path << "\n";
            }
        }

        void
        set_simulation_time( std::int64_t new_sim_gps ) noexcept
        {
            sim_time_ = new_sim_gps;
        }

    private:
        frame_generator_config          cfg_;
        std::deque< std::string >       frames_{ };
        std::int64_t                    sim_time_{ 1000000000 };
        std::vector< SimpleGenerator >& generators_;
        frame_utils::FrameData          fd_;
    };
    template < typename PredFunc >
    void
    simple_frame_loop( const frame_generator_config&   cfg,
                       std::vector< SimpleGenerator >& generators,
                       PredFunc                        done )
    {
        frame_generator g( cfg, generators );
        auto            start_time = std::chrono::steady_clock::now( );
        for ( auto i = 0; !done( ); ++i )
        {
            g.generate( );

            auto target_time = start_time + std::chrono::seconds( i + 1 );
            std::this_thread::sleep_until( target_time );
        }
    }

    template < typename GpsClock, typename PredFunc >
    void
    simple_frame_loop_gps_clock( const frame_generator_config&   cfg,
                                 std::vector< SimpleGenerator >& generators,
                                 GpsClock&                       clock,
                                 PredFunc                        done )
    {
        frame_generator g( cfg, generators );
        auto            cur_time = clock.now( ).sec;
        g.set_simulation_time( cur_time );
        for ( auto i = 0; !done( ); ++i )
        {
            g.generate( );

            auto target_time = cur_time + 1;
            auto gps_now = clock.now( ).sec;
            while ( gps_now < target_time )
            {
                std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
                gps_now = clock.now( ).sec;
            }
            g.set_simulation_time( target_time );
            cur_time = target_time;
        }
    }

} // namespace frame_utils

#endif // DAQD_STREAM_FRAME_UTILS_HH

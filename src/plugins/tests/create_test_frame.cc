//
// Created by jonathan.hanks on 9/30/21.
//

#include <daqd_stream/daqd_stream.hh>

#include <catch.hpp>

#include <chrono>
#include <sstream>
#include <vector>

#include "frame_plugin_internal.hh"
#include "frame_utils.hh"

#include <advligorts/daq_core.h>
#include <advligorts/daq_metadata.h>

void
verify_messages( const std::vector< frame_utils::SimpleGenerator >& generators,
                 pub_sub::SubMessage&                               msg,
                 int&                                               metaCount,
                 int&                                               dataCount )
{
    if ( ( msg.key( ) & 0x01 ) == 1 )
    {
        auto* metadata =
            reinterpret_cast< daq_multi_dcu_metadata_t* >( msg.data( ) );
        REQUIRE( metadata->header.magic == 0xffeeddcc );
        REQUIRE( metadata->header.version == 0 );
        REQUIRE( metadata->header.dcuCount == 1 );
        auto* dcu_header =
            reinterpret_cast< daq_dcu_metadata_header_t* >( metadata->data );
        REQUIRE( dcu_header->dcuId == 1 );
        REQUIRE( dcu_header->numChannels == generators.size( ) );
        REQUIRE( dcu_header->encodingMethod == METADATA_PLAIN );
        REQUIRE( dcu_header->encodedSize ==
                 sizeof( daq_channel_metadata_t ) * generators.size( ) );
        // capture of the checksum, make sure it is stable
        REQUIRE( dcu_header->checksum == 0x22c2b556 );

        auto* channel_data = reinterpret_cast< daq_channel_metadata_t* >(
            &metadata->data[ sizeof( daq_dcu_metadata_header_t ) ] );
        for ( auto i = 0; i < generators.size( ); ++i )
        {
            auto                    online = generators[ i ].metadata( );
            daq_channel_metadata_t& channel{ channel_data[ i ] };

            REQUIRE( online.name == channel.name );
            // REQUIRE(online.units == channel.units);
            REQUIRE( strcmp( channel.units, "" ) == 0 );
            REQUIRE( online.datarate == channel.datarate );
            REQUIRE(
                channel.datatype ==
                static_cast< unsigned int >( daqd_stream::DATA_TYPE::INT32 ) );
            REQUIRE( online.signal_gain == channel.signal_gain );
            REQUIRE( online.signal_slope == channel.signal_slope );
            REQUIRE( online.signal_offset == channel.signal_offset );
        }
        ++metaCount;
    }
    else
    {
        auto dc =
            reinterpret_cast< const daq_dc_data_t* >( msg.message( ).data( ) );
        REQUIRE( dc->header.dcuTotalModels == 1 );
        auto ref_data =
            reinterpret_cast< const unsigned char* >( dc->dataBlock );
        auto ref_data_end = ref_data + dc->header.fullDataBlockSize;

        auto buffer =
            std::vector< unsigned char >( 64 * 1024 * sizeof( std::uint32_t ) );
        for ( const auto& gen : generators )
        {
            const auto cur_seg_size = gen.bytes_per_16th( );
            if ( cur_seg_size < buffer.size( ) )
            {
                buffer.resize( cur_seg_size );
            }
            auto buf_end = gen.generate( buffer.data( ),
                                         dc->header.dcuheader[ 0 ].timeSec,
                                         dc->header.dcuheader[ 0 ].cycle );
            REQUIRE( ( buf_end - buffer.data( ) ) == cur_seg_size );
            REQUIRE( ( ref_data_end - ref_data ) >= cur_seg_size );
            REQUIRE( std::equal( buffer.data( ), buf_end, ref_data ) );
            ref_data += cur_seg_size;
        }
        REQUIRE( ref_data == ref_data_end );
        ++dataCount;
    }
}

TEST_CASE( "process test frame" )
{
    frame_utils::TempDir dir;

    auto generators = frame_utils::create_sample_channels( 1024 );
    frame_utils::FrameData fd( 1, generators );
    frame_utils::generate_one_second(
        generators, fd.channel_pointers_, 1000000000 );
    fd.write( dir.get( ), "X1-TEST", 1000000000, 1 );

    daqd_stream::plugins::frame_sub::info_and_path fInfo{
        { 1000000000, 1000000000 + 1, "X1-TEST" },
        frame_utils::create_path(
            dir.get( ).string( ), "X1-TEST", 1000000000, 1 ),
    };
    int                 metadata_msg_count{ 0 };
    int                 data_msg_count{ 0 };
    pub_sub::SubHandler verify_handler = [ &generators,
                                           &metadata_msg_count,
                                           &data_msg_count ](
                                             pub_sub::SubMessage msg ) {
        verify_messages( generators, msg, metadata_msg_count, data_msg_count );
    };
    auto start = std::chrono::steady_clock::now( );
    daqd_stream::plugins::frame_sub::process_frame( fInfo, verify_handler, 0 );
    auto end = std::chrono::steady_clock::now( );
    REQUIRE( metadata_msg_count == 1 );
    REQUIRE( data_msg_count == 16 );
    std::chrono::duration< double > elapsed_seconds = end - start;
    std::cout << "process time = " << elapsed_seconds.count( ) << "s\n";
}
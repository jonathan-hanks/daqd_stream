//
// Created by jonathan.hanks on 9/6/22.
//
#include "posix_mmap.hh"

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdexcept>

namespace
{
    class Fd_closer
    {
    public:
        explicit Fd_closer( int fd ) : fd_{ fd }
        {
        }
        ~Fd_closer( )
        {
            if ( fd_ >= 0 )
            {
                ::close( fd_ );
            }
        }

        int
        get( ) const noexcept
        {
            return fd_;
        }

    private:
        int fd_;
    };

    daqd_stream::detail::Posix_mmap::mapping_t
    mmap_file( const std::string& fname )
    {
        Fd_closer f( open( fname.c_str( ), O_RDONLY ) );
        if ( f.get( ) < 0 )
        {
            throw std::runtime_error( "Unable to open file for mmap" );
        }
        struct stat finfo
        {
        };
        if ( fstat( f.get( ), &finfo ) != 0 )
        {
            throw std::runtime_error( "Unable to stat file for mmap" );
        }
        daqd_stream::detail::Posix_mmap::mapping_t mapping{ };
        mapping.first =
            mmap( nullptr, finfo.st_size, PROT_READ, MAP_PRIVATE, f.get( ), 0 );
        if ( !mapping.first )
        {
            throw std::runtime_error( "Unable to mmap file" );
        }
        mapping.second = finfo.st_size;
        return mapping;
    }
} // namespace

namespace daqd_stream
{
    namespace detail
    {

        Posix_mmap::Posix_mmap( const std::string& name )
            : mapping_{ mmap_file( name ) }
        {
        }

        Posix_mmap::~Posix_mmap( )
        {
            unmap( );
        }

        void
        Posix_mmap::unmap( ) noexcept
        {
            if ( mapping_.first )
            {
                munmap( mapping_.first, mapping_.second );
            }
        }
    } // namespace detail
} // namespace daqd_stream
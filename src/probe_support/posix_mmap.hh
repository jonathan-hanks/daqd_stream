#ifndef DAQD_STREAM_PROBE_POSIX_MMAP_HH
#define DAQD_STREAM_PROBE_POSIX_MMAP_HH

#include <string>
#include <utility>

namespace daqd_stream
{
    namespace detail
    {
        class Posix_mmap
        {
        public:
            using mapping_t = std::pair< void*, size_t >;
            explicit Posix_mmap( const std::string& name );
            Posix_mmap( Posix_mmap&& other ) noexcept
                : mapping_{ std::move( other.mapping_ ) }
            {
                other.mapping_.first = nullptr;
            }
            ~Posix_mmap( );

            Posix_mmap&
            operator=( Posix_mmap&& other ) noexcept
            {
                unmap( );
                mapping_ = std::move( other.mapping_ );
                other.mapping_.first = nullptr;
                return *this;
            }

            void*
            get( ) noexcept
            {
                return mapping_.first;
            }

            const void*
            get( ) const noexcept
            {
                return mapping_.first;
            }

            template < typename T >
            T*
            get_as( ) noexcept
            {
                return reinterpret_cast< const T* >( get( ) );
            }

            template < typename T >
            const T*
            get_as( ) const noexcept
            {
                return reinterpret_cast< const T* >( get( ) );
            }

            std::size_t
            size( ) const noexcept
            {
                return mapping_.second;
            }

        private:
            void      unmap( ) noexcept;
            mapping_t mapping_;
        };
    } // namespace detail
} // namespace daqd_stream

#endif // DAQD_STREAM_PROBE_POSIX_MMAP_HH
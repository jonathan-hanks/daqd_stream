//
// Created by jonathan.hanks on 10/27/21.
//

#ifndef DAQD_STREAM_DAQ_METADATA_UTILS_HH
#define DAQD_STREAM_DAQ_METADATA_UTILS_HH

#include "daq_metadata.h"

#include <iostream>
#include <numeric>
#include <vector>

#include <zstd.h>

namespace daq_metadata
{
    template < typename F >
    auto
    unpack( F f ) -> auto
    {
        return [ f ]( const daq_dcu_metadata_header_t& header,
                      const void*                      data ) -> bool {
            if ( header.encodingMethod == METADATA_PLAIN )
            {
                auto* start_chan =
                    reinterpret_cast< const daq_channel_metadata_t* >( data );
                return f( header, start_chan, start_chan + header.numChannels );
            }
            if ( header.decodedSize !=
                 header.numChannels * sizeof( daq_channel_metadata_t ) )
            {
                return false;
            }
            std::vector< daq_channel_metadata_t > buffer( header.numChannels );
            auto                                  rc =
                ZSTD_decompress( reinterpret_cast< void* >( buffer.data( ) ),
                                 header.decodedSize,
                                 data,
                                 header.encodedSize );
            if ( ZSTD_isError( rc ) || rc != header.decodedSize )
            {
                return false;
            }
            auto start_chan =
                const_cast< const daq_channel_metadata_t* >( buffer.data( ) );
            return f( header, start_chan, start_chan + header.numChannels );
        };
    }

    template < typename F >
    bool
    visit_each_dcu( const daq_multi_dcu_metadata_t* metadata, F cb )
    {
        if ( !metadata )
        {
            return false;
        }
        auto* cur_header = reinterpret_cast< const daq_dcu_metadata_header_t* >(
            &metadata->data[ 0 ] );
        auto* end_boundary = &metadata->data[ metadata->header.dataBlockSize ];

        for ( auto i = 0; i < metadata->header.dcuCount; ++i )
        {
            auto* data = reinterpret_cast< const void* >( cur_header + 1 );
            if ( reinterpret_cast< const char* >( data ) +
                     cur_header->encodedSize >
                 end_boundary )
            {
                return false;
            }
            if ( !cb( *cur_header, data ) )
            {
                return false;
            }
            cur_header = reinterpret_cast< const daq_dcu_metadata_header_t* >(
                reinterpret_cast< const char* >( data ) +
                cur_header->encodedSize );
        }
        return true;
    }

    inline bool
    verify( daq_multi_dcu_metadata_t* metadata, std::size_t size )
    {
        if ( !metadata || size < sizeof( daq_multi_dcu_metadata_header_t ) )
        {
            std::cerr << "metadata size mismatch\n";
            return false;
        }
        if ( metadata->header.magic != DAQ_METADATA_MULTI_DCU_MAGIC ||
             metadata->header.version != 0 ||
             size < sizeof( daq_multi_dcu_metadata_header_t ) +
                     metadata->header.dataBlockSize ||
             metadata->header.dataBlockSize <
                 sizeof( daq_dcu_metadata_header_t ) *
                     metadata->header.dcuCount )
        {
            std::cerr << "metadata header issue\n";
            if ( metadata->header.magic != DAQ_METADATA_MULTI_DCU_MAGIC )
            {
                std::cerr << "magic value mismatch\n";
            }
            else if ( metadata->header.version != 0 )
            {
                std::cerr << "wrong version number\n";
            }
            else if ( size < ( sizeof( daq_multi_dcu_metadata_header_t ) +
                               metadata->header.dataBlockSize ) )
            {
                std::cerr << "size smaller than header + data block size\n";
            }
            else if ( metadata->header.dataBlockSize <
                      sizeof( daq_dcu_metadata_header_t ) *
                          metadata->header.dcuCount )
            {
                std::cerr << "data block size is wrong\n";
            }
            return false;
        }
        return visit_each_dcu(
            metadata,
            []( const daq_dcu_metadata_header_t& header,
                const void*                      data ) -> bool {
                if ( header.decodedSize !=
                     header.numChannels * sizeof( daq_channel_metadata_t ) )
                {
                    std::cerr << "metadata dcu size issue: " << header.dcuId
                              << " #chan " << header.numChannels << " size "
                              << header.decodedSize << "\n";
                    return false;
                }
                return true;
            } );
    }

    struct MetadataBuilder
    {
        std::vector< daq_dcu_metadata_header_t >             headers{ };
        std::vector< std::vector< daq_channel_metadata_t > > metadata{ };

        template < typename Channels, typename ToDaqMetaChan >
        bool
        append( int           dcuid,
                std::uint32_t config_crc,
                Channels&     channels,
                ToDaqMetaChan to_daq_meta )
        {
            if ( dcuid == 0 || channels.size( ) == 0 )
            {
                return false;
            }
            auto it = std::find_if(
                headers.begin( ),
                headers.end( ),
                [ dcuid ]( const daq_dcu_metadata_header_t& cur ) -> bool {
                    return cur.dcuId == dcuid;
                } );
            if ( it != headers.end( ) )
            {
                return false;
            }
            std::vector< daq_channel_metadata_t > metadata_chans(
                channels.size( ) );
            std::transform( channels.begin( ),
                            channels.end( ),
                            metadata_chans.begin( ),
                            to_daq_meta );

            daq_dcu_metadata_header_t header{ };
            header.dcuId = dcuid;
            header.numChannels = metadata_chans.size( );
            header.checksum = config_crc;
            header.encodedChecksum = METADATA_PLAIN;
            header.decodedSize = header.encodedSize =
                sizeof( daq_channel_metadata_t ) * metadata_chans.size( );
            header.encodedChecksum = config_crc;

            headers.emplace_back( header );
            try
            {
                metadata.emplace_back( std::move( metadata_chans ) );
            }
            catch ( ... )
            {
                headers.pop_back( );
                throw;
            }
            return true;
        }

        template < typename BlobBuilder >
        typename BlobBuilder::value_type
        to_binary_blob( BlobBuilder builder ) const
        {
            if ( headers.empty( ) )
            {
                return builder.allocate( 0 );
            }
            std::size_t header_size =
                sizeof( daq_dcu_metadata_header_t ) * headers.size( );
            std::size_t data_size = std::accumulate(
                metadata.begin( ),
                metadata.end( ),
                std::size_t( 0 ),
                []( std::size_t                                  cur_val,
                    const std::vector< daq_channel_metadata_t >& cur_element )
                    -> std::size_t {
                    return cur_val +
                        ( sizeof( daq_channel_metadata_t ) *
                          cur_element.size( ) );
                } );
            std::size_t total_size = sizeof( daq_multi_dcu_metadata_header_t ) +
                header_size + data_size;

            auto  data_blob = builder.allocate( total_size );
            auto* msg = reinterpret_cast< daq_multi_dcu_metadata_t* >(
                builder.data_pointer( data_blob ) );
            msg->header.magic = 0xffeeddcc;
            msg->header.version = 0;
            msg->header.dcuCount = headers.size( );
            msg->header.dataBlockSize = header_size + data_size;
            msg->header.dataBlockChecksum = 0;
            // msg->header.totalDataSize = header_size + data_size;

            auto* dcu_data = reinterpret_cast< daq_dcu_metadata_header_t* >(
                &msg->data[ 0 ] );
            for ( auto i = 0; i < headers.size( ); ++i )
            {
                *dcu_data = headers[ i ];
                ++dcu_data;
                auto* channels =
                    reinterpret_cast< daq_channel_metadata_t* >( dcu_data );
                auto& dcu_channels = metadata[ i ];
                std::copy(
                    dcu_channels.begin( ), dcu_channels.end( ), channels );
                channels += dcu_channels.size( );
                dcu_data =
                    reinterpret_cast< daq_dcu_metadata_header_t* >( channels );
            }
            return data_blob;
        }
    };
} // namespace daq_metadata

#endif // DAQD_STREAM_DAQ_METADATA_UTILS_HH
